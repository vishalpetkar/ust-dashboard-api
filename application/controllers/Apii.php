<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Uuid.php';
require "vendor/autoload.php";
use Razorpay\Api\Api;
use Restserver\Libraries\REST_Controller;

class Apii extends REST_Controller  {

	public function __construct() {
       parent::__construct();
       date_default_timezone_set('Asia/Kolkata');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
        $method = $_SERVER['REQUEST_METHOD'];
        /*if ($method == "OPTIONS") {
            die();
        }*/

       header('Access-Control-Allow-Origin: *');
	   header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	   header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

	   // Allow Post
	   if (isset($_SERVER['HTTP_ORIGIN'])) {
		    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		    header('Access-Control-Allow-Credentials: true');
		    header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		 
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");         
		 
		    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		 
		    exit(0);
		}
	    // ./ Allow Post /.

       $this->load->model('ApiModel');
       $this->load->model('GeneralModel');
	   $this->load->helper('errorname');

	   //check acces token fromheader start 
	   $last = $this->uri->total_segments();
	   $record_num = $this->uri->segment($last);
	   /*if(!($record_num == "users" && $this->input->server('REQUEST_METHOD') == 'POST') && $this->input->server('REQUEST_METHOD') != 'GET') {
			if(isset($this->input->request_headers()['Authorization']) && !empty($this->input->request_headers()['Authorization'])){
				if(!$this->ApiModel->auth($this->input->request_headers()['Authorization'])){
					geterror(401, "Unauthorized Access");
				}
			}else{
				geterror(401, "Unauthorized Access");
			}
		}*/
		//check acces token fromheader end
    }

	/*public function __construct() {
       parent::__construct();
       	date_default_timezone_set('Asia/Kolkata');
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
        $method = $_SERVER['REQUEST_METHOD'];
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");         
		    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		    // exit(0);
		}
	    // ./ Allow Post /.

       $this->load->model('ApiModel');
       $this->load->model('GeneralModel');
	   $this->load->helper('errorname');

	   //check acces token fromheader start 
	   $last = $this->uri->total_segments();
	   $record_num = $this->uri->segment($last);
	   if(!($record_num == "users" && $this->input->server('REQUEST_METHOD') == 'POST') && $this->input->server('REQUEST_METHOD') != 'GET') {
			if(isset($this->input->request_headers()['Authorization']) && !empty($this->input->request_headers()['Authorization'])){
				if(!$this->ApiModel->auth($this->input->request_headers()['Authorization'])){
					geterror(401, "Unauthorized Access");
				}
			}else{
				geterror(401, "Unauthorized Access");
			}
		}
		//check acces token fromheader end
    }*/
	// users crud start
	public function users_get($id = 0)
	{
		if (!empty($_GET['email']) && !empty($_GET['password'])) {
			$data = $this->GeneralModel->login($_GET['email'], $_GET['password']);
			$this->response($data, REST_Controller::HTTP_OK);
		} else {
			$data = $this->ApiModel->get($id,'users','id,name,lastname,email,mobile,address');
			$this->response($data, REST_Controller::HTTP_OK);
		}
	}

	public function users_post()
    {
    	$uuid = new Uuid();
		$accesstoken =  ($uuid->v4()).str_replace(' ', '-', strtolower(str_shuffle("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy")));

		$input = $this->post();
		$input['password'] = md5($input['password']);
		$input['accesstoken'] = $accesstoken;

		if ($this->GeneralModel->checkExistingEmail($input['email'])) {
			geterror(401, "Email already exist.");
		} else {
			$data = $this->ApiModel->post($input,'users');
		}
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function users_put($id=0)
    {
        $input = $this->put();
        if (!empty($_GET['action']) && $_GET['action']=="generate_otp") {
        	$data = $this->GeneralModel->generate_otp($input);
        } elseif(!empty($_GET['action']) && $_GET['action']=="reset_password") {
        	$data = $this->GeneralModel->reset_password($input);
        } else {
        	$data = $this->ApiModel->put($input,'users',$id);
        }
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function users_delete($id)
    {
        $data = $this->ApiModel->delete($id,'users');
		$this->response($data, REST_Controller::HTTP_OK);
    }
	// users crud end


    // Orders
    
    // ./ Orders /.


    // USERS ADDRESSES crud start
	public function addresses_get($id = 0)
	{
	    if($id) {
	        $data = $this->ApiModel->get($id,'addresses','id,name,mobile,address1,address2,city,state,is_default');   
	    } else {
	        $data = $this->GeneralModel->get_addresses();
	    }
	    if (!empty($_GET['default']) && !empty($_GET['user_id'])) {
	    	$user_id = $_GET['user_id'];
	    	$data[0]->distance = $this->GeneralModel->getDistanceOfCustomerFromRetailers($user_id);
	    }
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function addresses_post()
    {
    	$_GET['user_id'] = $this->post('user_id');
    	$data = $this->GeneralModel->get_addresses();
    	$input = $this->post();
    	if (empty($data)) {
    		$input['is_default'] = 1;
    	}
		$data = $this->ApiModel->post($input,'addresses');
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function addresses_put($id)
    {
        $input = $this->put();
        if (!empty($input['is_default'])) {
        	$this->GeneralModel->setNotDefaultAddress($input);
        }
		$data = $this->ApiModel->put($input,'addresses',$id);
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function addresses_delete($id)
    {
        $data = $this->ApiModel->delete($id,'addresses');
		$this->response($data, REST_Controller::HTTP_OK);
    }
	// USERS ADDRESSES crud END


    // Wishlist crud start
	public function wishproduct_get($user_id = 0)
	{
		$data = $this->GeneralModel->getWishproduct($user_id);
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function wishproduct_post()
    {
		$input 		= $this->post();
		$user_id 	= $input['user_id'];
		$product_id = $input['product_id'];
		$data 		= $this->ApiModel->post($input,'wishlist');
		$product_data = $this->ApiModel->get($product_id, "retailer_product");
		$product_data->wish_id = $data->id;
		$this->response($product_data, REST_Controller::HTTP_OK);
    }

    public function wishproduct_put($id)
    {
        $input = $this->put();
		$data = $this->ApiModel->put($input,'wishlist',$id);
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function wishproduct_delete($id)
    {
        $data = $this->ApiModel->delete($id,'wishlist');
        $data = $this->ApiModel->get($data->product_id, "retailer_product");
		$data->wish_id = null;
		$this->response($data, REST_Controller::HTTP_OK);
    }
	// Wishlist crud END

	// categories crud start
	public function categories_get($id = 0)
	{

		if(!empty($_GET['with']) && $_GET['with'] =="sliderpag"){
			// $data = $this->ApiModel->get($id,'category','id,name,image,slug',array("status"=>1));
			$data = $this->GeneralModel->getCategoryByLocation();
			$image = "";
			foreach ($data as $key => $value) {
				$image = "https://www.rostail.com/assets/uploads/cat_type_img/category_img/".$value->image;
				$value->image = $image;
			}
			if (!empty($data)) {
				$data = array_chunk($data,4);
				$last_arr = end($data);
				if (count($last_arr)<4) {
					for ($i=0; $i < (4 - count($last_arr)); $i++) {
						$last_arr[] = (object)(array('id'=>0));
					}
					$data[(count($data) - 1)] = $last_arr;
				}
			}

		}elseif($id && !empty($_GET['with']) && $_GET['with'] =="shopandsubcat") {

			$data = $this->ApiModel->get($id,'category','id,name,image,slug');
			// Shops and Subcategories
			$data->sellers = $this->GeneralModel->get_best_seller_by_category($data->id);
			foreach ($data->sellers as $key => $shop) {
				if (!empty($shop)) {
					$shop->rating   		= $this->GeneralModel->getShopRating($shop->retailer_id);
					$shop->total_products 	= $this->GeneralModel->getTotalProductByShop($shop->retailer_id);
					$shop->is_open 			= $this->GeneralModel->getShopOpenClose($shop->retailer_id);
					$data->sellers[$key]->subcategory = $this->GeneralModel->getSubcategoryOfRetailer($shop->retailer_id);
					foreach ($data->sellers[$key]->subcategory as $key => $subcat) {
						if (!empty($subcat->image)) {
							$image = $subcat->image;
							$subcat->image = "https://www.rostail.com/assets/uploads/cat_type_img/sub_cat_img/".$image;
						} else {
							$subcat->image = "https://www.rostail.com/assets/uploads/product-placeholder.gif";
						}
					}
				}
			}
			// ./ Shops and Subcategories /.

		}elseif(!empty($_GET['with']) && $_GET['with'] =="childcategories") {

			$subcategory_id = $_GET['subcategory_id'];
			$data = $this->ApiModel->get(0,'childcategory','id,child_cat_image as image,category_id,subcategory_id,name',array("subcategory_id"=>$subcategory_id));
		}elseif($id && !empty($_GET['with']) && $_GET['with'] =="subcategory" && !empty($_GET['andwith'])){

			$retailer_id = $_GET['retailer_id'];
			$data = $this->ApiModel->get($id,'category','id,name,image,slug');
			// $data->subcategory = $this->ApiModel->get(0,'subcategory','id,name,category_id',array("category_id"=>$data->id));
			// $data->childcategory = $this->ApiModel->get(0,'childcategory','id,category_id,subcategory_id,name',array("category_id"=>$data->id));
			$data->subcategory = $this->GeneralModel->getSubcategoryOfRetailer($retailer_id);
			foreach ($data->subcategory as $key => $subcat) {
				if (!empty($subcat->image)) {
					$image = $subcat->image;
					$subcat->image = "https://www.rostail.com/assets/uploads/cat_type_img/sub_cat_img/".$image;
				} else {
					$subcat->image = "https://www.rostail.com/assets/uploads/product-placeholder.gif";
				}
			}
			$data->childcategory = $this->GeneralModel->getChildcategoryOfRetailer($retailer_id);
			foreach ($data->childcategory as $key => $childcat) {
				if (!empty($childcat->image)) {
					$image = $childcat->image;
					$childcat->image = "https://www.rostail.com/assets/uploads/cat_type_img/child_cat_img/".$image;
				} else {
					$childcat->image = "https://www.rostail.com/assets/uploads/product-placeholder.gif";
				}
			}
		}elseif($id && !empty($_GET['with']) && $_GET['with'] =="subcategory"){

			$data = $this->ApiModel->get($id,'category','id,name,image,slug');
			$data->subcategory = $this->ApiModel->get(0,'subcategory','id,image,name,category_id',array("category_id"=>$data->id));
			foreach ($data->subcategory as $key => $subcat) {
				if (!empty($subcat->image)) {
					$image = $subcat->image;
					$subcat->image = "https://www.rostail.com/assets/uploads/cat_type_img/sub_cat_img/".$image;
				} else {
					$subcat->image = "https://www.rostail.com/assets/uploads/product-placeholder.gif";
				}
			}
		}elseif($id && !empty($_GET['with']) && $_GET['with'] =="products"){

			$data = $this->ApiModel->get($id,'category','id,name,slug');
			$data->products = $this->GeneralModel->get_feature_product($data->id);
			$data->sellers = $this->GeneralModel->get_best_seller($data->id);

		}elseif(!empty($_GET['with']) && $_GET['with'] =="products"){

			$data = $this->ApiModel->get($id,'category','id,name,image,slug',array("status"=>1));
			foreach ($data as $key => $value) {
				$data[$key]->products = $this->GeneralModel->get_feature_product($value->id,4);
				$data[$key]->subcategory = $this->ApiModel->get(0,'subcategory','id,image,name,category_id',array("category_id"=>$value->id));
				foreach ($data[$key]->subcategory as $key => $subcat) {
					if (!empty($subcat->image)) {
						$image = $subcat->image;
						$subcat->image = "https://www.rostail.com/assets/uploads/cat_type_img/sub_cat_img/".$image;
					} else {
						$subcat->image = "https://www.rostail.com/assets/uploads/product-placeholder.gif";
					}
				}
				$data[$key]->sellers = $this->GeneralModel->get_best_seller($value->id,4);
			}

		}else{
			$data = $this->ApiModel->get($id,'category','id,name,image,slug',array("status"=>1));
		}
		$this->response($data, REST_Controller::HTTP_OK);
	}
	// categories crud end

	public function review_post() {
		$input 	= $this->post();
		$data 	= $this->ApiModel->post($input,'rating');
		$this->response($data, REST_Controller::HTTP_OK);
	}

	// products crud start
	public function products_get($id = 0)
	{
		$data = [];
		if (!empty($id)) {
			$data 					= $this->ApiModel->get($id,'retailer_product','*, sp as actual_price',array("status"=>1));
			$data->is_returnable   	= $this->GeneralModel->getProductReturnableStatus($data->category_id);
			$data->rating        	= $this->GeneralModel->getProductRating($id);
			$data->slider_images 	= $this->GeneralModel->getProductImages($id, $data);
			$data->wish_id 			= $this->GeneralModel->getWishByProductId($id);
			$data->reviews 			= $this->GeneralModel->getReviewsByProduct($id);
			$data->similar_products = $this->GeneralModel->getSimilarproducts($data->retailer_id, $data->subcategory_id, $data->childcategory_id, $id);
			foreach ($data->similar_products as $key => $row) {
				$row->wish_id = $this->GeneralModel->getWishByProductId($row->id);
				if (!empty($row->footwear_size)) {
					$arr = explode(",", $row->footwear_size);
					$row->footwear_size_options = $this->GeneralModel->getData("tbl_footwear_size", 'id, value',$arr);
				}
				if (!empty($row->cloth_size)) {
					$arr = explode(",", $row->cloth_size);
					$row->cloth_size_options = $this->GeneralModel->getData("tbl_cloth_size", 'id, value',$arr);
				}
			}
		} else {
			if (!empty($_GET['filter']) && $_GET['filter']==true) {
				$with = array();
				$only = array();
				if (!empty($_GET['with'])) {
					$with = json_decode($_GET['with'], true);
				} elseif (!empty($_GET['only'])) {
					$arr_only = explode(",", $_GET['only']);
					foreach ($arr_only as $key => $arr_ele) {
						if (!empty($arr_ele)) {
							$only[] = $arr_ele;
						}
					}
				} elseif (!empty($_GET['vendor_id'])) {
					$with = array("retailer_id"=>$_GET['vendor_id']);
				}
				$data = $this->GeneralModel->get_products($with, $only);
			} elseif(!empty($_GET['search'])) {

				$data = $this->GeneralModel->get_products();

			} elseif (!empty($_GET['cart_products'])) {
				$data = [];
				$with = array();
				$only = array();
				if (!empty($_GET['user_id'])) {
					$user_id = $_GET['user_id'];
					$pro_arr = $this->GeneralModel->getCartproducts($user_id);
				}
				if (!empty($_GET['product_ids'])) {
					$pro_arr = explode(",", $_GET['product_ids']);
				}
				if (!empty($pro_arr)) {
					$data = $this->GeneralModel->get_products($with, $pro_arr, 'rp.*, rp.sp as sort_price, rp.sp as actual_price, w.id as wish_id, cat.return_order as is_returnable, c.id as cart_id, c.qty as quantity', false);
				}
				// echo "<pre>"; print_r($data); exit;
			}
			else {
				$with = "";
				if (!empty($_GET['with'])) {
					$with = json_decode($_GET['with'], true);
				}
				$data = $this->GeneralModel->get_products($with);
			}
		}

		// Add footer wear size, color, cloth size
		if (!empty($data)) {
			switch ($data) {
				case !empty($data->footwear_size):
					$arr = explode(",", $data->footwear_size);
					$data->footwear_size_options = $this->GeneralModel->getData("tbl_footwear_size", 'id, value',$arr);
					break;

				case !empty($data->cloth_size):
					$arr = explode(",", $data->cloth_size);
					$data->cloth_size_options = $this->GeneralModel->getData("tbl_cloth_size", 'id, value', $arr);
					break;

				default:
					foreach ($data as $key => $row) {
						if (!empty($row->footwear_size)) {
							$arr = explode(",", $row->footwear_size);
							$row->footwear_size_options = $this->GeneralModel->getData("tbl_footwear_size", 'id, value',$arr);
						}
						if (!empty($row->cloth_size)) {
							$arr = explode(",", $row->cloth_size);
							$row->cloth_size_options = $this->GeneralModel->getData("tbl_cloth_size", 'id, value',$arr);
						}
					}
					break;
			}

		}

		$this->response($data, REST_Controller::HTTP_OK);
	}
	// products crud end

	// banners start
	public function banners_get($id = 0)
	{
		$data = $this->ApiModel->get($id,'advertisement','images', array('status'=>1));
		foreach ($data as $key => $banner) {
			$img = $banner->images;
			$banner->images = "https://www.rostail.com/assets/uploads/products/thumb/".$img;
		}
		$this->response($data, REST_Controller::HTTP_OK);
	}
	// banners end

	// retailors crud start
	public function retailors_get($id = 0, $subCatId=0, $childCatId=0) {

		$data 					= $this->ApiModel->get($id,'retailer','*');
		$data->rating   		= $this->GeneralModel->getShopRating($id);
		$data->total_products 	= $this->GeneralModel->getTotalProductByShop($id, $subCatId, $childCatId);
		$data->is_open 			= $this->GeneralModel->getShopOpenClose($id);
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function slug_retailors_get($slug = "") {

		$data = $this->ApiModel->get(0,'retailer','*', array("slug"=>$slug));
		$this->response($data, REST_Controller::HTTP_OK);
	}
	// retailors crud end

	public function offers_get() {
		$data = $this->ApiModel->get(0,'promo_code','*');
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function checkoffer_get($code="", $user_id="") {
		$data = $this->GeneralModel->checkOffer($code, $user_id);
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function shops_categories_get() {

		// Shops and Subcategories
		$data['shops'] = $this->GeneralModel->get_best_seller();
		foreach ($data['shops'] as $key => $shop) {
			if (!empty($shop)) {
				// $data['shops'][$key]->subcategory = $this->ApiModel->get(0,'subcategory','id,name,category_id',array("category_id"=>$shop->category_id));
				$shop->rating   = $this->GeneralModel->getShopRating($shop->retailer_id);
				$shop->reviews  = $this->GeneralModel->getReviewsByShop($shop->retailer_id);
				$shop->is_open 	= "Closed";
				$opningtime 	= $shop->opningtime;
				$closingtime 	= $shop->closingtime;
				$opentime 		= date('H:i:s',strtotime("$opningtime PM"));
				$closetime 		= date('H:i:s',strtotime("$closingtime PM"));
				if(	$opningtime!=NULL && $closingtime!=NULL &&
					(strtotime(date('d-m-Y H:i:s')) > strtotime("$opningtime AM")  && 
					strtotime(date('d-m-Y H:i:s')) < strtotime("$closingtime PM"))) {
				    $shop->is_open = "Open";
				}
				if ($opningtime==NULL || $closingtime==NULL) {
					$shop->is_open = "";
				}
				$subcat_ids = $shop->main_subcategory_id.",".$shop->subcategory_id;
				$subcat_arr = [];
		        $subcat_arr = explode(",", $subcat_ids);
				$data['shops'][$key]->subcategory = $this->GeneralModel->getSubcategoryOfCategory($subcat_arr);
				foreach ($data['shops'][$key]->subcategory as $key => $value) {
					if (!empty($value->image)) {
						$image = $value->image;
						$value->image = "https://www.rostail.com/assets/uploads/cat_type_img/sub_cat_img/".$image;
					} else {
						$value->image = "https://www.rostail.com/assets/uploads/product-placeholder.gif";
					}
				}
			}
		}
		// ./ Shops and Subcategories /.

		// Categories with Subcategories
		// $data['categories'] = $this->ApiModel->get(0,'category','id,image,name,slug');
		$data['categories'] = $this->GeneralModel->getCategoryByLocation();
		foreach ($data['categories'] as $key => $value) {
			if (!empty($value)) {
				// $value->subcategory = $this->ApiModel->get(0,'subcategory','id,name,category_id',array("category_id"=>$value->id));
				$value->subcategory = $this->GeneralModel->getSubcategoryOfCategoryByRetailer($value->id);
				foreach ($value->subcategory as $key => $subcat) {
					if (!empty($subcat->image)) {
						$image = $subcat->image;
						$subcat->image = "https://www.rostail.com/assets/uploads/cat_type_img/sub_cat_img/".$image;
					} else {
						$subcat->image = "https://www.rostail.com/assets/uploads/product-placeholder.gif";
					}
				}
			}
		}
		// ./ Categories with Subcategories /.

		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function cart_get($user_id = 0)
	{
		$pro_arr = $this->GeneralModel->getCartproducts($user_id);
		if (!empty($pro_arr)) {
        	$_GET['with'] = implode(",", $pro_arr);
        	$data = $this->products_get();
        }
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function cart_put($id)
    {
        $input = $this->put();
		$data = $this->ApiModel->put($input,'cart',$id);
		$this->response($data, REST_Controller::HTTP_OK);
    }

	public function cart_post()
    {
		$input = $this->post();
		if (!empty($_GET['repeat'])) {
			$data = $this->GeneralModel->repeatOrder($input['user_id'], $input['order_id']);
		} else {
			$data = $this->ApiModel->post($input,'cart');
		}
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function cart_delete($id)
    {
    	$data = [];
    	if (!empty($_GET['flag']) && $_GET['flag']=="deleteall") {
    		$this->GeneralModel->deleteCart();
    	} else {
        	$data = $this->ApiModel->delete($id,'cart');
    	}
		$this->response($data, REST_Controller::HTTP_OK);
    }

	public function orders_get($user_id, $order_id=0) {
		$data = $this->GeneralModel->getOrders($user_id, $order_id);
		foreach ($data as $key => $value) {
			$value->order_details 	= $this->GeneralModel->getOrderProducts($value->id);
			$value->order_shops 	= $this->GeneralModel->getOrderShops($value->id);
			$value->status_id		= $value->status;
			$value->status 			= $this->getStatus($value->status);
			if ($value->promo_amount_type=="Percentage") {
				$tot_amnt = 0;
				foreach ($value->order_details as $key => $pro_value) {
					$tot_amnt = $tot_amnt + $pro_value->amount;
				}
				$percent_amount = ($tot_amnt * $value->promo_amount) / 100;
				$value->promo_amount =  ceil($percent_amount);
			}
		}
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function orders_post()
    {
        $data = $this->post();
        $this->GeneralModel->addOrder($data);
		$this->response($data, REST_Controller::HTTP_OK);
    }

    function is_decimal( $val )
	{
	    if(is_numeric( $val ) && floor( $val ) != $val) {
	    	return str_replace(".","",$val);
	    } else {
	    	return $val."00";
	    }
	}

    public function order_id_post() {
    	$order_data = $this->post();
    	$api 		= new Api($this->config->item('key_id'), $this->config->item('secret_key'));
		$amount     = $this->is_decimal($order_data['amount']);
		$order  	= $api->order->create(
					array(
						'receipt' 			=> rand(), 
						'amount' 			=> $amount, 
						'currency' 			=> 'INR', 
						'payment_capture' 	=> true)
					); // Creates order
		$data['order_id'] = $order->id;
		$this->response($data, REST_Controller::HTTP_OK);
    }

    function getStatus($status_id=0) {
    	$order_status = array(
    		0=>"",
	        1=>"Order Placed", 
	        2=>"Picked Up", 
	        3=>"Delivered", 
	        4=>"Delivery Boy Accepted", 

	        5=>"Cancelled", 
	        6=>"Return Order Request", 
	        7=>"Return Order Picked Up", 
	        8=>"Return Order Completed", 
	        10=>"Retailer Accepted Order"
	    );
	    return $order_status[$status_id];
    }

    public function faq_get()
	{
		$data = $this->ApiModel->get(0,'faq','*');
		foreach ($data as $key => $value) {
			$value->show = false;
		}
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function reviews_get($id, $type) {
		$data = $this->GeneralModel->getReviews($id, $type);
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function return_product_post() {
		$input 	= $this->post();
		$data 	= $this->GeneralModel->returnProduct($input);
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function cancle_product_post() {
		$input 	= $this->post();
		$data 	= $this->GeneralModel->cancleProduct($input);
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function aboutus_get() {
		$data = $this->GeneralModel->getAboutus();
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function privacy_get() {
		$data = $this->GeneralModel->getPrivacy();
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function delivery_charges_get() {
		$charges = $this->GeneralModel->getDeliveryCharges();
		$data = [];
		foreach ($charges as $key => $value) {
			if (!empty($value['variable'])) {
				$data[$value['variable']] = $value['value1'];
			}
		}
		$this->response($data, REST_Controller::HTTP_OK);
	}

	// Delivery APIS
	public function deliveryboy_get($id = 0) {
		if (!empty($_GET['email']) && !empty($_GET['password'])) {
			$data = $this->GeneralModel->login_delivery($_GET['email'], $_GET['password']);
			$output = array("status"=>"success", "user"=>$data);
		} else {
			$data = $this->ApiModel->get($id,'delivery_user','id,name,email,mobile,address');
			$output = array("status"=>"success", "user"=>$data);
		}
		$this->response($output, REST_Controller::HTTP_OK);
	}

	public function deliveryboy_post() {

		$input = $this->post();
		if (empty($input['email'])) {
			geterror(401, "Email id can't be null.");
			exit;
		}
		if (empty($input['password'])) {
			geterror(401, "Password can't be null.");
			exit;
		}
		$input['password'] = md5($input['password']);

		if ($this->GeneralModel->checkDeliveryEmail($input['email'])) {
			geterror(401, "Email already exist.");
		} else {
			$data = $this->ApiModel->post($input,'delivery_user');
		}
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function deliveryboy_put($id=0)
    {
        $input = $this->put();
        if (!empty($_GET['action']) && $_GET['action']=="generate_otp") {
        	$data = $this->GeneralModel->generate_otp($input);
        } elseif(!empty($_GET['action']) && $_GET['action']=="reset_password") {
        	$data = $this->GeneralModel->reset_password($input);
        } else {
        	$data = $this->ApiModel->put($input,'delivery_user',$id);
        }
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function deliveryboy_delete($id)
    {
        $data = $this->ApiModel->delete($id,'delivery_user');
		$this->response($data, REST_Controller::HTTP_OK);
    }

	public function delivery_boy_response_post() {
		$input 	= $this->post();
		$check_status = $this->GeneralModel->deliveryBoyResponse($input);
		if ($check_status) {
			$data = $this->ApiModel->get($input['orderId'],'order','*');
			$data->order_details 	= $this->GeneralModel->getOrderProducts($data->id);
			$data->order_shops 	= $this->GeneralModel->getOrderShops($data->id);
			$data->status_id		= $data->status;
			$data->status 			= $this->getStatus($data->status);
			// $data->customer			= $this->ApiModel->get($data->user_id, "users", "name, lastname, email, mobile, address");
			if ($data->promo_amount_type=="Percentage") {
				$tot_amnt = 0;
				foreach ($data->order_details as $key => $pro_value) {
					$tot_amnt = $tot_amnt + $pro_value->amount;
				}
				$percent_amount = ($tot_amnt * $data->promo_amount) / 100;
				$data->promo_amount =  ceil($percent_amount);
			}
		}
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function pushupdate_post()
    {
		$input = $this->post();
		$data = [];
		$this->GeneralModel->updatePush($input);
		$this->response($data, REST_Controller::HTTP_OK);
    }

	public function temp_users_get($id = 0)
	{
		if (!empty($_GET['email']) && !empty($_GET['password'])) {
			$data = $this->GeneralModel->templogin($_GET['email'], $_GET['password']);
			$this->response($data, REST_Controller::HTTP_OK);
		} else {
			$data = $this->ApiModel->get($id,'temp_users','*');
			$this->response($data, REST_Controller::HTTP_OK);
		}
	}

	public function temp_users_post()
    {
		$input = $this->post();
		$input['password'] = md5($input['password']);

		if ($this->GeneralModel->tempcheckExistingEmail($input['email'])) {
			geterror(401, "Email already exist.");
		} else {
			$data = $this->ApiModel->post($input,'temp_users');
			$data->status = "success";
		}
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function temp_blogs_get($id=0)
	{
		$data = $this->ApiModel->get($id,'tbl_blogs','*');
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function temp_blogs_post() {

		$input = $this->post();
		$data = $this->ApiModel->post($input,'tbl_blogs');
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function temp_blogs_put($id=0)
    {
        $input = $this->put();
        $like_data = $this->ApiModel->get($id,'tbl_blogs','total_like');
        $input['total_like'] = $like_data->total_like + 1;
        $data = $this->ApiModel->put($input,'tbl_blogs',$id);
		$this->response($data, REST_Controller::HTTP_OK);
    }
}