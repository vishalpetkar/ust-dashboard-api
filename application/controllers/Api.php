<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

require APPPATH . '/libraries/xlsx-reader-master/lib/Reader.php';
require APPPATH . '/libraries/xlsx-reader-master/lib/NumberFormat.php';
require APPPATH . '/libraries/xlsx-reader-master/lib/SharedStringsConfiguration.php';
require APPPATH . '/libraries/xlsx-reader-master/lib/RelationshipData.php';
require APPPATH . '/libraries/xlsx-reader-master/lib/OoxmlReader.php';
require APPPATH . '/libraries/xlsx-reader-master/lib/RelationshipElement.php';
require APPPATH . '/libraries/xlsx-reader-master/lib/Worksheet.php';
require APPPATH . '/libraries/xlsx-reader-master/lib/SharedStrings.php';
require APPPATH . '/libraries/xlsx-reader-master/lib/NumberFormatSection.php';
require APPPATH . '/libraries/xlsx-reader-master/lib/NumberFormatToken.php';

use Restserver\Libraries\REST_Controller;
use Aspera\Spreadsheet\XLSX\Reader;
use Aspera\Spreadsheet\XLSX\NumberFormat;
use Aspera\Spreadsheet\XLSX\SharedStringsConfiguration;
use Aspera\Spreadsheet\XLSX\RelationshipData;
use Aspera\Spreadsheet\XLSX\OoxmlReader;
use Aspera\Spreadsheet\XLSX\RelationshipElement;
use Aspera\Spreadsheet\XLSX\Worksheet;
use Aspera\Spreadsheet\XLSX\SharedStrings;
use Aspera\Spreadsheet\XLSX\NumberFormatSection;
use Aspera\Spreadsheet\XLSX\NumberFormatToken;

class Api extends REST_Controller  {

	public function __construct() {
       parent::__construct();
       $this->load->model('ApiModel');
       $this->load->model('GeneralModel');
    }

	public function employee_get($id = 0)
	{
	    if($id) {
	        $data = $this->ApiModel->get($id);
	    } else {
	        $data = $this->GeneralModel->get_employees();
	    }
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function employee_post()
    {
    	move_uploaded_file($_FILES['employee_list']['tmp_name'], 'assets/'.$_FILES['employee_list']['name']);
		$options = array(
		    'TempDir'                    => 'C:\xampp\htdocs\dashboardapi\temp',
		    'SkipEmptyCells'             => false,
		    'ReturnDateTimeObjects'      => true,
		    'SharedStringsConfiguration' => new SharedStringsConfiguration(),
		    'CustomFormats'              => array(20 => 'hh:mm')
		);
		$reader = new Reader($options);
		$reader->open('assets/'.$_FILES['employee_list']['name']);
		$i = 0;
		$all_emp = [];
		$columns = $this->GeneralModel->getAllColumns();
		foreach ($reader as $row) {
			if ($i == 0) {
				$i++;
				continue;
			}
			$emp_data = [];
			$emp_columns = $columns;
			// print_r($row);
		    foreach ($row as $key => $col) {
		    	$next_col = next($emp_columns);
		    	
		    	switch ($next_col) {

		    		case 'nationality_id':
		    			$isId = $this->GeneralModel->get('country', 'nationality', $col);
			    		if (!$isId) {
			    			$isId = $this->GeneralModel->add('country', array('nationality' => $col ));
			    		}
		    			break;

		    		case 'country_working':
		    			$isId = $this->GeneralModel->get('country', 'title', $col);
			    		if (!$isId) {
			    			$isId = $this->GeneralModel->add('country', array('title' => $col ));
			    		}
		    			break;

	    			case 'primary_skill':
	    				$isId = $this->GeneralModel->get('skills', 'title', $col);
			    		if (!$isId) {
			    			$isId = $this->GeneralModel->add('skills', array('title' => $col ));
			    		}
	    				break;

					case 'secondary_skill':
	    				$isId = $this->GeneralModel->get('skills', 'title', $col);
			    		if (!$isId) {
			    			$isId = $this->GeneralModel->add('skills', array('title' => $col ));
			    		}
	    				break;

	    			case 'currency':
	    				$isId = $this->GeneralModel->get('currency', 'title', $col);
			    		if (!$isId) {
			    			$isId = $this->GeneralModel->add('currency', array('title' => $col ));
			    		}
	    				break;

	    			case 'currency':
	    				$isId = $this->GeneralModel->get('currency', 'title', $col);
			    		if (!$isId) {
			    			$isId = $this->GeneralModel->add('currency', array('title' => $col ));
			    		}
	    				break;

		    		default:
		    			# code...
		    			break;
		    	}

		    	if ($col instanceof DateTime) {
				  $col = $col->format('Y-m-d H:i:s');
				}
		    	$emp_data[$next_col] = $col;
		    }
		    array_push($all_emp, $emp_data);
		}
		$reader->close();
    	$this->GeneralModel->addEmployees($all_emp);
		$this->response([], REST_Controller::HTTP_OK);
    }

}

