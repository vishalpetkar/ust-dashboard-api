<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/Uuid.php';
use Restserver\Libraries\REST_Controller;

class Api extends REST_Controller  {

	public function __construct() {
       parent::__construct();

        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
        $method = $_SERVER['REQUEST_METHOD'];
        /*if ($method == "OPTIONS") {
            die();
        }*/

       header('Access-Control-Allow-Origin: *');
	   header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	   header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');

	   // Allow Post
	   if (isset($_SERVER['HTTP_ORIGIN'])) {
		    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		    header('Access-Control-Allow-Credentials: true');
		    header('Access-Control-Max-Age: 86400');    // cache for 1 day
		}
		if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		 
			if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
		        header("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");         
		 
		    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
		        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		 
		    exit(0);
		}
	    // ./ Allow Post /.

       $this->load->model('ApiModel');
       $this->load->model('GeneralModel');
	   $this->load->helper('errorname');

	   //check acces token fromheader start 
	   $last = $this->uri->total_segments();
	   $record_num = $this->uri->segment($last);
	   /*if(!($record_num == "users" && $this->input->server('REQUEST_METHOD') == 'POST') && $this->input->server('REQUEST_METHOD') != 'GET') {
			if(isset($this->input->request_headers()['Authorization']) && !empty($this->input->request_headers()['Authorization'])){
				if(!$this->ApiModel->auth($this->input->request_headers()['Authorization'])){
					geterror(401, "Unauthorized Access");
				}
			}else{
				geterror(401, "Unauthorized Access");
			}
		}*/
		//check acces token fromheader end
    }
	// users crud start
	public function users_get($id = 0)
	{
		if (!empty($_GET['email']) && !empty($_GET['password'])) {
			$data = $this->GeneralModel->login($_GET['email'], $_GET['password']);
			$this->response($data, REST_Controller::HTTP_OK);
		} else {
			$data = $this->ApiModel->get($id,'users','id,name,lastname,email,mobile,address');
			$this->response($data, REST_Controller::HTTP_OK);
		}
	}

	public function users_post()
    {
    	$uuid = new Uuid();
		$accesstoken =  ($uuid->v4()).str_replace(' ', '-', strtolower(str_shuffle("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy")));

		$input = $this->post();
		$input['password'] = md5($input['password']);
		$input['accesstoken'] = $accesstoken;

		if ($this->GeneralModel->checkExistingEmail($input['email'])) {
			geterror(401, "Email already exist.");
		} else {
			$data = $this->ApiModel->post($input,'users');
		}
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function users_put($id=0)
    {
        $input = $this->put();
        if (!empty($_GET['action']) && $_GET['action']=="generate_otp") {
        	$data = $this->GeneralModel->generate_otp($input);
        } elseif(!empty($_GET['action']) && $_GET['action']=="reset_password") {
        	$data = $this->GeneralModel->reset_password($input);
        } else {
        	$data = $this->ApiModel->put($input,'users',$id);
        }
		$this->response($data, REST_Controller::HTTP_OK);
    }
	
    public function users_delete($id)
    {
        $data = $this->ApiModel->delete($id,'users');
		$this->response($data, REST_Controller::HTTP_OK);
    }
	// users crud end


    // Orders
    
    // ./ Orders /.


    // USERS ADDRESSES crud start
	public function addresses_get($id = 0)
	{
	    if($id) {
	        $data = $this->ApiModel->get($id,'addresses','id,name,mobile,address1,address2,city,state,is_default');   
	    } else {
	        $data = $this->GeneralModel->get_addresses();
	    }
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function addresses_post()
    {
    	$_GET['user_id'] = $this->post('user_id');
    	$data = $this->GeneralModel->get_addresses();
    	$input = $this->post();
    	if (empty($data)) {
    		$input['is_default'] = 1;
    	}
		$data = $this->ApiModel->post($input,'addresses');
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function addresses_put($id)
    {
        $input = $this->put();
        if (!empty($input['is_default'])) {
        	$this->GeneralModel->setNotDefaultAddress($input);
        }
		$data = $this->ApiModel->put($input,'addresses',$id);
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function addresses_delete($id)
    {
        $data = $this->ApiModel->delete($id,'addresses');
		$this->response($data, REST_Controller::HTTP_OK);
    }
	// USERS ADDRESSES crud END


    // Wishlist crud start
	public function wishproduct_get($user_id = 0)
	{
		$data = $this->GeneralModel->getWishproduct($user_id);
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function wishproduct_post()
    {
		$input 		= $this->post();
		$user_id 	= $input['user_id'];
		$product_id = $input['product_id'];
		$data 		= $this->ApiModel->post($input,'wishlist');
		$product_data = $this->ApiModel->get($product_id, "retailer_product");
		$product_data->wish_id = $data->id;
		$this->response($product_data, REST_Controller::HTTP_OK);
    }

    public function wishproduct_put($id)
    {
        $input = $this->put();
		$data = $this->ApiModel->put($input,'wishlist',$id);
		$this->response($data, REST_Controller::HTTP_OK);
    }

    public function wishproduct_delete($id)
    {
        $data = $this->ApiModel->delete($id,'wishlist');
        $data = $this->ApiModel->get($data->product_id, "retailer_product");
		$data->wish_id = null;
		$this->response($data, REST_Controller::HTTP_OK);
    }
	// Wishlist crud END


	// categories crud start
	public function categories_get($id = 0)
	{

		if(!empty($_GET['with']) && $_GET['with'] =="sliderpag"){
			$data = $this->ApiModel->get($id,'category','id,name,image,slug',array("status"=>1));
			$image = "";
			foreach ($data as $key => $value) {
				$image = "https://www.rostail.com/assets/uploads/products/thumb/".$value->image;
				$value->image = $image;  
			}
			$data = array_chunk($data,4);

		}elseif($id && !empty($_GET['with']) && $_GET['with'] =="shopandsubcat") {

			$data = $this->ApiModel->get($id,'category','id,name,image,slug');
			// Shops and Subcategories
			$data->sellers = $this->GeneralModel->get_best_seller_by_category($data->id);
			foreach ($data->sellers as $key => $shop) {
				if (!empty($shop)) {
					$data->sellers[$key]->subcategory = $this->ApiModel->get(0,'subcategory','id,name,category_id',array("category_id"=>$shop->category_id));
					foreach ($data->sellers[$key]->subcategory as $key => $subcategory) {
						$subcategory->image = $this->GeneralModel->getImageForSubcategory($shop->retailer_id, $shop->category_id, $subcategory->id);
					}
				}
			}
			// ./ Shops and Subcategories /.

		}elseif(!empty($_GET['with']) && $_GET['with'] =="childcategories") {

			$subcategory_id = $_GET['subcategory_id'];
			$data = $this->ApiModel->get(0,'childcategory','id,category_id,subcategory_id,name',array("subcategory_id"=>$subcategory_id));
		}elseif($id && !empty($_GET['with']) && $_GET['with'] =="subcategory" && !empty($_GET['andwith'])){

			$retailer_id = $_GET['retailer_id'];
			$data = $this->ApiModel->get($id,'category','id,name,image,slug');
			// $data->subcategory = $this->ApiModel->get(0,'subcategory','id,name,category_id',array("category_id"=>$data->id));
			// $data->childcategory = $this->ApiModel->get(0,'childcategory','id,category_id,subcategory_id,name',array("category_id"=>$data->id));
			$data->subcategory = $this->GeneralModel->getSubcategoryOfRetailer($retailer_id);
			$data->childcategory = $this->GeneralModel->getChildcategoryOfRetailer($retailer_id);

		}elseif($id && !empty($_GET['with']) && $_GET['with'] =="subcategory"){

			$data = $this->ApiModel->get($id,'category','id,name,image,slug');
			$data->subcategory = $this->ApiModel->get(0,'subcategory','id,name,category_id',array("category_id"=>$data->id));
			
		}elseif($id && !empty($_GET['with']) && $_GET['with'] =="products"){

			$data = $this->ApiModel->get($id,'category','id,name,slug');
			$data->products = $this->GeneralModel->get_feature_product($data->id);
			$data->sellers = $this->GeneralModel->get_best_seller($data->id);

		}elseif(!empty($_GET['with']) && $_GET['with'] =="products"){

			$data = $this->ApiModel->get($id,'category','id,name,image,slug',array("status"=>1));
			foreach ($data as $key => $value) {
				$data[$key]->products = $this->GeneralModel->get_feature_product($value->id,4);
				$data[$key]->subcategory = $this->ApiModel->get(0,'subcategory','id,name,category_id',array("category_id"=>$value->id));
				$data[$key]->sellers = $this->GeneralModel->get_best_seller($value->id,4);
			}

		}else{
			$data = $this->ApiModel->get($id,'category','id,name,image,slug',array("status"=>1));
		}
		$this->response($data, REST_Controller::HTTP_OK);
	}
	// categories crud end

	public function review_post() {
		$input 	= $this->post();
		$data 	= $this->ApiModel->post($input,'rating');
		$this->response($data, REST_Controller::HTTP_OK);
	}

	// products crud start
	public function products_get($id = 0)
	{
		$data = [];
		if (!empty($id)) {
			$data 					= $this->ApiModel->get($id,'retailer_product','*, (amount_cgst+amount_sgst+actual_price) as actual_price',array());
			$data->rating        	= $this->GeneralModel->getProductRating($id);
			$data->slider_images 	= $this->GeneralModel->getProductImages($id, $data);
			$data->wish_id 			= $this->GeneralModel->getWishByProductId($id);
			$data->reviews 			= $this->GeneralModel->getReviewsByProduct($id);
			$data->similar_products = $this->GeneralModel->getSimilarproducts($data->retailer_id, $data->subcategory_id, $data->childcategory_id, $id);
			foreach ($data->similar_products as $key => $row) {
				$row->wish_id = $this->GeneralModel->getWishByProductId($row->id);
			}
		} else {
			if (!empty($_GET['filter']) && $_GET['filter']==true) {
				$with = array();
				$only = array();
				if (!empty($_GET['with'])) {
					$with = json_decode($_GET['with'], true);
				} elseif (!empty($_GET['only'])) {
					$arr_only = explode(",", $_GET['only']);
					foreach ($arr_only as $key => $arr_ele) {
						if (!empty($arr_ele)) {
							$only[] = $arr_ele;
						}
					}
				} elseif (!empty($_GET['vendor_id'])) {
					$with = array("retailer_id"=>$_GET['vendor_id']);
				}
				$data = $this->GeneralModel->get_products($with, $only);
			} elseif(!empty($_GET['search'])) {
				$user_id = $_GET['user_id'];
				$sql = "
				SELECT rp.*, (amount_cgst+amount_sgst+actual_price) as actual_price, w.id as wish_id
				FROM tbl_retailer_product rp
				Left Join tbl_wishlist w on w.product_id=rp.id and w.user_id=$user_id
				WHERE 
				status=1 And 
				(REPLACE(product_name, \"'\",\"\") like '%".str_replace("'", "", $this->db->escape_like_str($_GET['value']))."%' ESCAPE '!' Or 
				REPLACE(product_description, \"'\",\"\") like '%".str_replace("'", "", $this->db->escape_like_str($_GET['value']))."%' ESCAPE '!' Or 
				REPLACE(specification, \"'\",\"\") like '%" .str_replace("'", "", $this->db->escape_like_str($_GET['value']))."%' ESCAPE '!')
				order by rp.id
				limit 100";
				$query = $this->db->query($sql, array($_GET['value'], $_GET['value'], $_GET['value']));
				$data = $query->result();
				$data = !empty($_GET['value'])? $data : array();
			} else {
				$with = "";
				if (!empty($_GET['with'])) {
					$with = json_decode($_GET['with'], true);
				}
				$data = $this->GeneralModel->get_products($with);
			}
		}

		// Add footer wear size, color, cloth size
		// foreach ($data as $key => $row) {
			if (!empty($data)) {
				switch ($data) {
					case !empty($data->footwear_size):
					
						$arr = explode(",", $data->footwear_size);
						$data->footwear_size_options = $this->GeneralModel->getData("tbl_footwear_size", 'id, value',$arr);
						break;

					case !empty($data->cloth_size):
						$arr = explode(",", $data->cloth_size);
						$data->cloth_size_options = $this->GeneralModel->getData("tbl_cloth_size", 'id, value', $arr);
						break;
				}	
			}
		// }

		$this->response($data, REST_Controller::HTTP_OK);
	}
	// products crud end

	// banners start
	public function banners_get($id = 0)
	{
		$data = $this->ApiModel->get($id,'advertisement','images', array('status'=>1));
		foreach ($data as $key => $banner) {
			$img = $banner->images;
			$banner->images = "https://www.rostail.com/assets/uploads/products/thumb/".$img;
		}
		$this->response($data, REST_Controller::HTTP_OK);
	}
	// banners end

	// retailors crud start
	public function retailors_get($id = 0)
	{
		$data = $this->ApiModel->get($id,'retailer','*');
		$this->response($data, REST_Controller::HTTP_OK);
	}
	// retailors crud end

	public function offers_get() {
		$data = $this->ApiModel->get(0,'promo_code','*');
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function checkoffer_get($code="") {
		$data = $this->GeneralModel->checkOffer($code);
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function shops_categories_get() {

		// Shops and Subcategories
		$data['shops'] = $this->GeneralModel->get_best_seller();
		foreach ($data['shops'] as $key => $shop) {
			if (!empty($shop)) {
				// $data['shops'][$key]->subcategory = $this->ApiModel->get(0,'subcategory','id,name,category_id',array("category_id"=>$shop->category_id));
				$subcat_ids = $shop->main_subcategory_id.",".$shop->subcategory_id;
				$subcat_arr = [];
		        $subcat_arr = explode(",", $subcat_ids);
				$data['shops'][$key]->subcategory = $this->GeneralModel->getSubcategoryOfCategory($subcat_arr);
				foreach ($data['shops'][$key]->subcategory as $key => $subcategory) {
					$subcategory->image = $this->GeneralModel->getImageForSubcategory($shop->retailer_id, $shop->category_id, $subcategory->id);
				}
			}
		}
		// ./ Shops and Subcategories /.

		// Categories with Subcategories
		$data['categories'] = $this->ApiModel->get(0,'category','id,image,name,slug',array("status"=>1));
		foreach ($data['categories'] as $key => $value) {
			if (!empty($value)) {
				// $value->subcategory = $this->ApiModel->get(0,'subcategory','id,name,category_id',array("category_id"=>$value->id));
				$value->subcategory = $this->GeneralModel->getSubcategoryOfCategoryByRetailer($value->id);
				foreach ($value->subcategory as $key => $value) {
					$value->image = $this->GeneralModel->getImageForSubcategory(0, $shop->category_id, $subcategory->id);
				}
			}
		}
		// ./ Categories with Subcategories /.

		$this->response($data, REST_Controller::HTTP_OK);
	}

	/*function orders_get($user_id, $order_id=0) {
		$data = $this->GeneralModel->getOrders($user_id, $order_id);
		$this->response($data, REST_Controller::HTTP_OK);
	}*/

	public function orders_get($user_id, $order_id=0) {
		$data = $this->GeneralModel->getOrders($user_id, $order_id);
		foreach ($data as $key => $value) {
			$value->order_details = $this->GeneralModel->getOrderProducts($value->id);
		}
		$this->response($data, REST_Controller::HTTP_OK);
	}

	public function orders_post()
    {
        $data = $this->post();
        $this->GeneralModel->addOrder($data);
		$this->response($data, REST_Controller::HTTP_OK);
    }
}