<?php
class ApiModel extends CI_Model {

    public function get($id = 0,$table,$select = "*",$where=array(),$limit=0)
    {
        $this->db->select($select);
        $this->db->from($table);
        if(!empty($where)){
            $this->db->where($where);
        }
        if($limit){
            $this->db->limit($limit);
        }
        if($id){
                $this->db->where('id',$id);
                $res = $this->db->get()->row();
                return $res ? $res : new stdClass();
        }else{
                return $this->db->get()->result();
        }
    }

    public function post($data,$table)
    {
        if(isset($data['id'])){
                unset($data['id']);
        }
        $data['created'] = date('Y-m-d H:i:s');
        $data['modified'] = date('Y-m-d H:i:s');
       $this->db->insert($table, $data);
       return $this->get($this->db->insert_id(),$table);
    }

    public function put($data,$table,$id)
    {
        if(isset($data['id'])){
                unset($data['id']);
        }
       $this->db->update($table, $data, array('id' => $id));
       return $this->get($id,$table);
    }

    public function delete($id,$table)
    {
        $data = $this->get($id,$table);
        $this->db->delete($table, array('id'=>$id));
        return $data;
    }

    public function auth($token)
    {
        $this->db->select('*');
        $this->db->from("users");
        $this->db->where('accesstoken',$token);
        return $this->db->get()->num_rows();;
    }

}