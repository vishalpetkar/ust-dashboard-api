<?php
class GeneralModel extends CI_Model {

    public function login($email, $password)
    {
        $this->db->select("*");
        $this->db->from("users");
        $this->db->where('email',$email);
        $this->db->where('password', md5($password));
        $res = $this->db->get()->result();
        if (!empty($res)) {
            return $res[0];
        } else {
            return new stdClass();
        }
    }

    public function generate_otp($data) {
        $this->db->select('id');
        $this->db->from("users");
        $this->db->where('mobile',$data['mobile']);
        $res = $this->db->get()->result();
        if (!empty($res)) {
            $data['code'] = rand(0,9999);
            $this->db->where('id', $res[0]->id);
            $this->db->update('users', array('otp'=> $data['code']));
            $data['status'] = "200";

            // Send SMS
            $message = urlencode('OTP for forgot password is '.$data['code'].'.');
            $password = '123456'; 
            $senderid = 'ROSTAL';
            $mobile = '91'.$data['mobile'];
            $baseurl = "http://sms.indiatext.in/api/mt/SendSMS?user=ROSTAL&password=".$password;
            $url = $baseurl.'&senderid='.$senderid.'&channel=Trans&DCS=0&flashsms=0&number='.$mobile.'&text='.$message.'&route=35';    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POST, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch); 
            curl_close($ch);
            // ./ Send SMS /.

            return $data;
        } else {
            $data['status'] = "404";
            return $data;
        }
    }

    public function reset_password($data) {
        $this->db->select('id');
        $this->db->from("users");
        $this->db->where('mobile',$data['mobile']);
        $this->db->where('otp',$data['otp']);
        $res = $this->db->get()->result();
        if (!empty($res)) {
            $this->db->where('id', $res[0]->id);
            $this->db->update('users', array('password'=> md5($data['password'])));
            $data['status'] = "200";
            return $data;
        } else {
            $data['status'] = "404";
            return $data;
        }
    }

    public function checkExistingEmail($email)
    {
        $this->db->select('email');
        $this->db->from("users");
        $this->db->where('email',$email);
        $res = $this->db->get()->result();
        return count($res);
    }
    public function withSubCat()
    {
        $this->db->select('id,name,slug,imgurl');
        $this->db->from("category");
        return $this->db->get()->result();
    }

    public function get_feature_product($cid = 0,$limt = 0)
    {

        $this->db->select('retailer_product.id,retailer_product.retailer_id,retailer_product.product_name,retailer_product.mrp,retailer_product.discount,retailer_product.images,retailer_product.status,feature_product.id as product_id group by created order by id desc' );
        $this->db->from('feature_product');
        $this->db->join('retailer_product','retailer_product.id=feature_product.product_id');
        if($cid){
            $this->db->where('retailer_product.category_id',$cid);   
            if($limt != 0){
                $this->db->limit($limt);
            }
            $this->db->order_by('rand()');
        }
        $query=$this->db->get();
        return $query->result();

    }

    public function get_best_seller($cid = 0,$limt = 0)
    {
        $this->db->select('retailer.main_subcategory_id,retailer.subcategory_id,retailer.status,retailer.banner_image,retailer.shop_name,retailer.category_id,retailer.slug,seller.retailer_id as retailer_id');
        $this->db->from('seller');
        $this->db->join('retailer','retailer.id=seller.retailer_id');
        if($cid) {
            $this->db->where("(retailer.category_id=$cid or FIND_IN_SET($cid,category_multiple))");
            if($limt != 0){
                $this->db->limit($limt);
            }
            $this->db->order_by('rand()');
        }
        $this->db->where('retailer.status', 1);
        $query=$this->db->get();
        // echo $this->db->last_query();

        return $query->result();
    }

    public function get_best_seller_by_category($cid = 0,$limt = 0) {
        $this->db->select('retailer.main_subcategory_id,retailer.subcategory_id,retailer.status,retailer.banner_image,retailer.shop_name,retailer.category_id,retailer.slug,retailer.id as retailer_id');
        $this->db->from('retailer');
        if($cid) {
            $this->db->where("(retailer.category_id=$cid or FIND_IN_SET($cid,category_multiple))");
            if($limt != 0){
                $this->db->limit($limt);
            }
        }
        $this->db->where('retailer.status', 1);
        $query=$this->db->get();
        return $query->result();
    }

    public function get_products($where_array=array(), $only=array(), $select='rp.*, (amount_cgst+amount_sgst+actual_price) as sort_price, (amount_cgst+amount_sgst+actual_price) as actual_price, w.id as wish_id') {

        if (!empty($_GET['sub_cat_id'])) {
            $retailer_arr = [];
            $subcat_id = $_GET['sub_cat_id'];
            $this->db->select('id');
            $this->db->from('retailer');
            $this->db->where("(FIND_IN_SET($subcat_id,main_subcategory_id)) and status=1");
            $query=$this->db->get();
            $retailer_data = $query->result();
            foreach ($retailer_data as $key => $row) {
                $retailer_arr[] = $row->id;
            }
            $this->db->where_in("retailer_id", $retailer_arr);
        }
        /* elseif (!empty($_GET['child_cat_id'])) {
            $retailer_arr = [];
            $childcat_id = $_GET['child_cat_id'];
            $this->db->select('id');
            $this->db->from('retailer');
            $this->db->where("(FIND_IN_SET($childcat_id,childcategory_id)) and status=1");
            $query=$this->db->get();
            $retailer_data = $query->result();
            echo $this->db->last_query();
            foreach ($retailer_data as $key => $row) {
                $retailer_arr[] = $row->id;
            }
            $this->db->where_in("retailer_id", $retailer_arr);
        }*/

        $this->db->select($select);
        $this->db->from('retailer_product rp');
        if (!empty($where_array)) {
            $this->db->where($where_array);
        }
        if (!empty($only)) {
            $this->db->where_in("rp.id", $only);
        }
        if (!empty($_GET['limit'])) {
            $limit = $_GET['limit'];
            $offset = $_GET['offset'];
            $this->db->limit($limit, $limit*$offset);
        }
        $user_id=0;
        if (!empty($_GET['user_id']) && $_GET['user_id']!="null") {
            $user_id = $_GET['user_id'];
        }
        if (!empty($_GET['retailer_id'])) {
            $this->db->where("retailer_id", $_GET['retailer_id']);
        }
        if (!empty($_GET['cat_id'])) {
            $this->db->where("category_id", $_GET['cat_id']);
        }
        if (!empty($_GET['sub_cat_id'])) {
            $this->db->where("subcategory_id", $_GET['sub_cat_id']);
        }
        if (!empty($_GET['child_cat_id'])) {
            $this->db->where("childcategory_id", $_GET['child_cat_id']);
        }
        if (!empty($_GET['sort_by'])) {
            $this->db->order_by("sort_price", $_GET['sort_by']);
        }
        $this->db->join('wishlist w', "w.product_id=rp.id and w.user_id=".$user_id, 'left');
        $this->db->where("status", 1);
        $query=$this->db->get();
        return $query->result();
    }

    public function getSimilarproducts($retailer_id, $subid, $childid, $prodid) {
        $query = $this->db->query('select * from tbl_retailer_product where  (subcategory_id='.$subid.' or childcategory_id='.$childid.') and retailer_id='.$retailer_id.' and id!='.$prodid.' limit 8');
        return $query->result();
    }

    public function getWishproduct($user_id) {
        $this->db->select('wishlist.id as wish_id, retailer_product.*');
        $this->db->from('tbl_wishlist');
        $this->db->join('retailer_product', "wishlist.product_id=tbl_retailer_product.id", 'left');
        $this->db->where("wishlist.user_id", $user_id);
        $query=$this->db->get();
        return $query->result();
    }

    public function getProductImages($product_id, $product_data) {


        $this->db->select('name');
        $this->db->from('tbl_product_gallery');
        $this->db->where("product_id", $product_id);
        $query=$this->db->get();
        $data = $query->result();
        $img_arr = [];
        if (!empty($product_data->images)) {
        	$img_arr[] = $product_data->images;
        }
        foreach ($data as $key => $img) {
            $img_arr[] = $img->name;
        }
        return $img_arr;
    }

    public function getWishByProductId($product_id) {
        $this->db->select('id');
        $this->db->from('tbl_wishlist');
        $this->db->where("product_id", $product_id);
        $query=$this->db->get();
        $data = $query->result();
        if (!empty($data)) {
            return $data[0]->id;
        } else {
            return 0;
        }
    }

    public function getImageForSubcategory($shop_id=0, $cat_id, $subcat_id) {
        return "https://www.witneyandmason.com/wp-content/themes/witneyandmason/images/product-placeholder.gif";
        $this->db->select('pg.name as image');
        $this->db->from('tbl_retailer_product rp');
        $this->db->join('tbl_product_gallery pg', "pg.product_id=rp.id", 'left');
        $this->db->where("rp.subcategory_id", $subcat_id);
        $this->db->where("rp.category_id", $cat_id);
        
        if ($shop_id!=0) {
            $this->db->where("rp.retailer_id", $shop_id);
        }
        
        $this->db->order_by("RAND()");
        $this->db->limit(1);
        $query=$this->db->get();
        $data = $query->result();
        if (!empty($data[0]->image)) {
            return "https://www.rostail.com/assets/uploads/products/thumb/".$data[0]->image;
        } else {
            return "https://www.witneyandmason.com/wp-content/themes/witneyandmason/images/product-placeholder.gif";
        }
    }

    function getOrders($user_id, $order_id) {
        $this->db->select('o.delivery_charge, o.promo_amount, o.created, o.id, o.total_amount, o.total_product, o.status, o.address_name, o.address_mobile, o.address1, o.address2');
        $this->db->from('tbl_order o');
        $this->db->where("o.user_id", $user_id);
        $this->db->order_by("created", "desc");
        if (!empty($order_id)) {
            $this->db->where("o.id", $order_id);
        }
        $query=$this->db->get();
        return $query->result();
    }

    function getOrderProducts($order_id) {
        $this->db->select('od.product_id, od.amount, od.unit_amount, od.qty, od.discount, od.totaldiscount, rp.product_name, rp.mrp, rp.actual_price, rp.images');
        $this->db->from('tbl_order_details od');
        $this->db->join('tbl_retailer_product rp', 'od.product_id=rp.id', 'left');
        $this->db->where("order_id", $order_id);
        $query=$this->db->get();
        return $query->result();
    }

    function checkOffer($code) {
        $this->db->select('*');
        $this->db->from('promo_code');
        $this->db->where("coupon_code", $code);
        $query=$this->db->get();
        $data = $query->result();
        if (empty($data)) {
            $data = new stdClass();
            $data->valid = false;
            return $data;
        } else {
            $data[0]->valid = true;
            return $data[0];
        }
    }

    function get_addresses() {
        $this->db->select('*');
        $this->db->from('tbl_addresses');
        $this->db->where("user_id", $_GET['user_id']);
        if (!empty($_GET['default'])) {
            $this->db->where("is_default", 1);
        }
        $query=$this->db->get();
        return $query->result();
    }

    function setNotDefaultAddress($data) {
        $this->db->update('tbl_addresses', array('is_default'=>0), array('user_id' => $data['user_id']));
    }

    function addOrder($data) {

        $order_data['user_id']              = $data['user_id'];
        $order_data['promocode_id']         = $data['promocode_id'];
        $order_data['address_name']         = $data['address_name'];
        $order_data['address_mobile']       = $data['address_mobile'];
        $order_data['address1']             = $data['address1'];
        $order_data['address2']             = $data['address2'];
        $order_data['total_amount']         = $data['total_amount'];
        $order_data['total_product']        = $data['total_product'];
        $order_data['status']               = $data['status'];
        $order_data['promo_amount']         = $data['promo_amount'];
        $order_data['promo_amount_type']    = $data['promo_amount_type'];
        $order_data['delivery_charge']      = $data['delivery_charge'];
        $order_data['created']              = date('Y-m-d H:i:s');

        $this->db->insert('tbl_order', $order_data);
        $order_id = $this->db->insert_id();

        foreach ($data['order_details'] as $key => $order_product) {

            $this->db->select('(amount_cgst+amount_sgst+actual_price) as actual_price');
            $this->db->from('tbl_retailer_product');
            $this->db->where("id", $order_product['product_id']);
            $query=$this->db->get();
            $product_data = $query->result();

            $order_detail['order_id']           = $order_id;
            $order_detail['product_id']         = $order_product['product_id'];
            $order_detail['amount']             = ($product_data[0]->actual_price * $order_product['qty']) ;
            $order_detail['unit_amount']        = $product_data[0]->actual_price;
            $order_detail['qty']                = $order_product['qty'];
            $order_detail['discount']           = $order_product['discount'];
            $order_detail['totaldiscount']      = $order_product['totaldiscount'];
            if (!empty($order_product['cloth_size'])) {
                $order_detail['cloth_size']     = $order_product['cloth_size'];
            }
            if (!empty($order_product['footwear_size'])) {
                $order_detail['footwear_size']      = $order_product['footwear_size'];
            }
            $order_detail['status']             = $order_product['status'];
            $this->db->insert('tbl_order_details', $order_detail);
        }
    }

    function getProductRating($product_id) {
        $this->db->select('(SUM(review)/count(id)) as rating');
        $this->db->from('tbl_rating');
        $this->db->where("product_id", $product_id);
        $this->db->group_by('product_id');
        $query=$this->db->get();
        $data = $query->result();
        if (!empty($data)) {
           return $data[0]->rating; 
        } else {
           return 0;
        }
    }

    function getReviewsByProduct($product_id) {
        $this->db->select('r.id,CONCAT(u.name, " ", u.lastname) as user_name,r.review,r.comment,r.created');
        $this->db->from('tbl_rating r');
        $this->db->join("tbl_users u", "u.id=r.user_id", "left");
        $this->db->where("product_id", $product_id);
        $query=$this->db->get();
        return $query->result();
    }

    function getSubcategoryOfCategory($subcat_arr) {
        $this->db->select('id,name,category_id');
        $this->db->from('subcategory');
        $this->db->where_in("id", $subcat_arr);
        $query=$this->db->get();
        return $query->result();
    }

    function getData($table, $fields, $id_arr) {
        $this->db->select($fields);
        $this->db->from($table);
        $this->db->where_in("id", $id_arr);
        $query=$this->db->get();
        return $query->result();
    }

    function getSubcategoryOfCategoryByRetailer($category_id) {

        // Get subcategories related to active retailer
        $subcat_arr = [];
        $this->db->select('id,main_subcategory_id,subcategory_id');
        $this->db->from('retailer');
        $this->db->where("(retailer.category_id=$category_id)");
        $this->db->where("status", 1);
        $query=$this->db->get();
        $subcat_data = $query->result();
        foreach ($subcat_data as $key => $row_subcat) {
            $subcat_arr = array_merge($subcat_arr, explode(",", $row_subcat->main_subcategory_id));
            $subcat_arr = array_merge($subcat_arr, explode(",", $row_subcat->subcategory_id));   
        }

        $this->db->select('id,name,category_id');
        $this->db->from('subcategory');
        $this->db->where_in("id", $subcat_arr);
        $query=$this->db->get();
        return $query->result();
    }

    function getSubcategoryOfRetailer($retailer_id) {
        $subcat_arr = [];
        $this->db->select('id,main_subcategory_id,subcategory_id');
        $this->db->from('retailer');
        $this->db->where("id", $retailer_id);
        $query=$this->db->get();
        $subcat_data = $query->result();
        foreach ($subcat_data as $key => $row_subcat) {
            $subcat_arr = array_merge($subcat_arr, explode(",", $row_subcat->main_subcategory_id));
            $subcat_arr = array_merge($subcat_arr, explode(",", $row_subcat->subcategory_id));   
        }

        $this->db->select('id,name,category_id');
        $this->db->from('subcategory');
        $this->db->where_in("id", $subcat_arr);
        $query=$this->db->get();
        return $query->result();
    }

    function getChildcategoryOfRetailer($retailer_id) {
        $childcat_arr = [];
        $this->db->select('id,childcategory_id');
        $this->db->from('retailer');
        $this->db->where("id", $retailer_id);
        $query=$this->db->get();
        $childcat_data = $query->result();
        foreach ($childcat_data as $key => $row_childcat) {
            $childcat_arr = array_merge($childcat_arr, explode(",", $row_childcat->childcategory_id));
        }
        if (!empty($childcat_arr)) {
            $this->db->select('id,category_id,subcategory_id,name');
            $this->db->from('childcategory');
            $this->db->where_in("id", $childcat_arr);
            $query=$this->db->get();
            return $query->result();
        } else {
            return new stdClass();
        }
        
    }
}
