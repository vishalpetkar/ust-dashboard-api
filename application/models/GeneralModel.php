<?php
class GeneralModel extends CI_Model {

    var $table_name = 'employees';

    public function getAllColumns() {
        return $this->db->list_fields($this->table_name);
    }

    public function addEmployees($data) {
        $this->db->insert_batch($this->table_name, $data);
    }

    public function get_employees() {
        $this->db->select("*");
        $this->db->from($this->table_name);
        return $this->db->get()->result();
    }

    public function get($table, $column, $value) {
        $this->db->select("*");
        $this->db->from($table);
        $this->db->where($column => $value);
        $data = $this->db->get()->result();
        if (count($data)) {
            return $data[0]->id;
        } else {
            return 0;
        }
    }

    public function add($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

}
