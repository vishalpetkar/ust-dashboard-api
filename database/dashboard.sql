-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `dash_band_or_grade`;
CREATE TABLE `dash_band_or_grade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `dash_country`;
CREATE TABLE `dash_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `dash_country` (`id`, `title`, `nationality`) VALUES
(1,	'India',	'Indian'),
(2,	'Malaysia',	'Malaysian');

DROP TABLE IF EXISTS `dash_currency`;
CREATE TABLE `dash_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `dash_employees`;
CREATE TABLE `dash_employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vertical` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `account` varchar(50) NOT NULL,
  `account_manager` varchar(50) NOT NULL,
  `pid` varchar(50) NOT NULL,
  `project_name` varchar(100) NOT NULL,
  `sow_start_date` date NOT NULL,
  `sow_end_date` date NOT NULL,
  `uid` int(11) NOT NULL,
  `emp_name` varchar(100) NOT NULL,
  `nationality_id` int(11) DEFAULT NULL,
  `gender` enum('m','f') NOT NULL DEFAULT 'm',
  `date_of_joining` date NOT NULL,
  `total_experience` int(11) NOT NULL,
  `primary_skill` int(11) DEFAULT NULL,
  `secondary_skill` int(11) DEFAULT NULL,
  `country_working` int(11) DEFAULT NULL,
  `band_grade` int(11) DEFAULT NULL,
  `currency` int(11) DEFAULT NULL,
  `billing_rate` int(11) NOT NULL DEFAULT 0,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `working_days` int(11) NOT NULL DEFAULT 0,
  `leave_days` int(11) NOT NULL DEFAULT 0,
  `net_days` int(11) NOT NULL DEFAULT 0,
  `overtime_rate` int(11) NOT NULL,
  `total_billing` int(11) NOT NULL,
  `billable` tinyint(4) NOT NULL,
  `billed` tinyint(4) NOT NULL,
  `reason` text NOT NULL,
  `reason_for_unbilled` text NOT NULL,
  `reason_for_billable` text NOT NULL,
  `sst` int(11) NOT NULL,
  `po` int(11) NOT NULL,
  `po_balance` int(11) NOT NULL,
  `invoice_number` int(11) NOT NULL,
  `invoice_amount` int(11) NOT NULL,
  `upload_date` date NOT NULL,
  `pay_date` date NOT NULL,
  `piad` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `nationality_id` (`nationality_id`),
  KEY `primary_skill` (`primary_skill`),
  KEY `secondary_skill` (`secondary_skill`),
  KEY `country_working` (`country_working`),
  KEY `band_grade` (`band_grade`),
  KEY `currency` (`currency`),
  CONSTRAINT `dash_employees_ibfk_1` FOREIGN KEY (`nationality_id`) REFERENCES `dash_country` (`id`),
  CONSTRAINT `dash_employees_ibfk_2` FOREIGN KEY (`primary_skill`) REFERENCES `dash_skills` (`id`),
  CONSTRAINT `dash_employees_ibfk_3` FOREIGN KEY (`secondary_skill`) REFERENCES `dash_skills` (`id`),
  CONSTRAINT `dash_employees_ibfk_4` FOREIGN KEY (`country_working`) REFERENCES `dash_country` (`id`),
  CONSTRAINT `dash_employees_ibfk_5` FOREIGN KEY (`band_grade`) REFERENCES `dash_band_or_grade` (`id`),
  CONSTRAINT `dash_employees_ibfk_6` FOREIGN KEY (`currency`) REFERENCES `dash_currency` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `dash_skills`;
CREATE TABLE `dash_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- 2020-10-19 03:37:20
