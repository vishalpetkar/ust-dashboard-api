(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vendor-vendor-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/vendor/vendor/vendor.component.html":
/*!*******************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/vendor/vendor/vendor.component.html ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-list Lines=\"none\" slot=\"start\">\n      <ion-item>\n        <h5><b>Best Sellers</b></h5>\n      </ion-item>\n    </ion-list>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<!-- Yellow Box Store -->\n<ion-card class=\"box box-yellow\">\n  <ion-grid>\n    <ion-row>\n      <ion-col class=\"heading heading-white\" size=\"8\">Best Seller | Crockery</ion-col>\n      <ion-col><button>View All</button></ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-item Lines=\"none\">\n    <app-store-thumb></app-store-thumb>\n  </ion-item>\n  <ion-item Lines=\"none\">\n    <app-store-thumb></app-store-thumb>\n  </ion-item>\n  <ion-item Lines=\"none\">\n    <app-store-thumb></app-store-thumb>\n  </ion-item>\n  <ion-item Lines=\"none\">\n    <app-store-thumb></app-store-thumb>\n  </ion-item>\n</ion-card>\n<!-- ./ Yellow Box Store /. -->\n\n\n<!-- Blue Box Store-->\n<ion-card class=\"box box-blue\">\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"8\" class=\"heading heading-white\">Best Seller | Fashion</ion-col>\n      <ion-col><button>View All</button></ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-item Lines=\"none\">\n    <app-store-thumb></app-store-thumb>\n  </ion-item>\n  <ion-item Lines=\"none\">\n    <app-store-thumb></app-store-thumb>\n  </ion-item>\n  <ion-item Lines=\"none\">\n    <app-store-thumb></app-store-thumb>\n  </ion-item>\n  <ion-item Lines=\"none\">\n    <app-store-thumb></app-store-thumb>\n  </ion-item>\n</ion-card>\n<!-- ./ Blue Box Store /. -->\n</ion-content>"

/***/ }),

/***/ "./src/app/vendor/vendor-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/vendor/vendor-routing.module.ts ***!
  \*************************************************/
/*! exports provided: VendorRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorRoutingModule", function() { return VendorRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _vendor_vendor_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vendor/vendor.component */ "./src/app/vendor/vendor/vendor.component.ts");




var routes = [
    { path: '', pathMatch: '', component: _vendor_vendor_component__WEBPACK_IMPORTED_MODULE_3__["VendorComponent"] }
];
var VendorRoutingModule = /** @class */ (function () {
    function VendorRoutingModule() {
    }
    VendorRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], VendorRoutingModule);
    return VendorRoutingModule;
}());



/***/ }),

/***/ "./src/app/vendor/vendor.module.ts":
/*!*****************************************!*\
  !*** ./src/app/vendor/vendor.module.ts ***!
  \*****************************************/
/*! exports provided: VendorModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorModule", function() { return VendorModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _vendor_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vendor-routing.module */ "./src/app/vendor/vendor-routing.module.ts");
/* harmony import */ var _vendor_vendor_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./vendor/vendor.component */ "./src/app/vendor/vendor/vendor.component.ts");
/* harmony import */ var _commonviews_commonviews_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../commonviews/commonviews.module */ "./src/app/commonviews/commonviews.module.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");







var VendorModule = /** @class */ (function () {
    function VendorModule() {
    }
    VendorModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_vendor_vendor_component__WEBPACK_IMPORTED_MODULE_4__["VendorComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _vendor_routing_module__WEBPACK_IMPORTED_MODULE_3__["VendorRoutingModule"],
                _commonviews_commonviews_module__WEBPACK_IMPORTED_MODULE_5__["CommonviewsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_6__["IonicModule"]
            ]
        })
    ], VendorModule);
    return VendorModule;
}());



/***/ }),

/***/ "./src/app/vendor/vendor/vendor.component.scss":
/*!*****************************************************!*\
  !*** ./src/app/vendor/vendor/vendor.component.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZlbmRvci92ZW5kb3IvdmVuZG9yLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/vendor/vendor/vendor.component.ts":
/*!***************************************************!*\
  !*** ./src/app/vendor/vendor/vendor.component.ts ***!
  \***************************************************/
/*! exports provided: VendorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VendorComponent", function() { return VendorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var VendorComponent = /** @class */ (function () {
    function VendorComponent() {
    }
    VendorComponent.prototype.ngOnInit = function () { };
    VendorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-vendor',
            template: __webpack_require__(/*! raw-loader!./vendor.component.html */ "./node_modules/raw-loader/index.js!./src/app/vendor/vendor/vendor.component.html"),
            styles: [__webpack_require__(/*! ./vendor.component.scss */ "./src/app/vendor/vendor/vendor.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], VendorComponent);
    return VendorComponent;
}());



/***/ })

}]);
//# sourceMappingURL=vendor-vendor-module-es5.js.map