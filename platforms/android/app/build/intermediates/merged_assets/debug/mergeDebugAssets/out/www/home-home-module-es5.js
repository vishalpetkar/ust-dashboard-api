(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html":
/*!***************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/home/home.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-list Lines=\"none\">\n      <ion-item>\n        <ion-label slot=\"end\" class=\"heading\">Curent Location</ion-label>\n        <ion-icon name=\"locate\" slot=\"end\"></ion-icon>\n          <!-- <ion-input  placeholder=\"Find Shops and Products\"></ion-input>  -->\n      </ion-item>\n    </ion-list>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n    \n  <!-- Banner -->\n  <ion-slides autoplay=\"5000\" loop=\"true\" speed=\"500\" pager=\"true\" class=\"home-slider-banner\">\n    <ion-slide>\n      <img src=\"https://mir-s3-cdn-cf.behance.net/project_modules/disp/806fe417784937.5676ca4a7b59a.jpg\"/>\n    </ion-slide>\n    <ion-slide>\n      <img src=\"https://mir-s3-cdn-cf.behance.net/project_modules/disp/9945f517784937.562c5a7b51769.jpg\"/>\n    </ion-slide>\n    <ion-slide>\n      <img src=\"https://mir-s3-cdn-cf.behance.net/project_modules/disp/fdaa0417784937.5676ca4a7c67e.jpg\"/>\n    </ion-slide>\n  </ion-slides>\n  <!-- ./ Banner /. -->\n\n  <app-categorymenu></app-categorymenu>\n\n  <!-- Blue Box Store-->\n  <ion-card class=\"box box-blue\">\n    <ion-grid>\n      <ion-row>\n        <ion-col size=\"8\" class=\"heading heading-white\">Best Seller | Fashion</ion-col>\n        <ion-col><button>View All</button></ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-item Lines=\"none\">\n      <app-store-thumb></app-store-thumb>\n    </ion-item>\n    <ion-item Lines=\"none\">\n      <app-store-thumb></app-store-thumb>\n    </ion-item>\n    <ion-item Lines=\"none\">\n      <app-store-thumb></app-store-thumb>\n    </ion-item>\n    <ion-item Lines=\"none\">\n      <app-store-thumb></app-store-thumb>\n    </ion-item>\n  </ion-card>\n  <!-- ./ Blue Box Store /. -->\n\n  <!-- Yellow Box Products-->\n  <ion-card class=\"box box-yellow\">\n    <ion-grid>\n      <ion-row>\n        <ion-col class=\"heading heading-white\" size=\"8\">Featured Products | Fashion</ion-col>\n        <ion-col><button>View All</button></ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-card>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n    </ion-card>\n  </ion-card>\n  <!-- ./ Yellow Box Products /. -->\n\n  <!-- Yellow Box Store -->\n  <ion-card class=\"box box-yellow\">\n    <ion-grid>\n      <ion-row>\n        <ion-col class=\"heading heading-white\" size=\"8\">Best Seller | Crockery</ion-col>\n        <ion-col><button>View All</button></ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-item Lines=\"none\">\n      <app-store-thumb></app-store-thumb>\n    </ion-item>\n    <ion-item Lines=\"none\">\n      <app-store-thumb></app-store-thumb>\n    </ion-item>\n    <ion-item Lines=\"none\">\n      <app-store-thumb></app-store-thumb>\n    </ion-item>\n    <ion-item Lines=\"none\">\n      <app-store-thumb></app-store-thumb>\n    </ion-item>\n  </ion-card>\n  <!-- ./ Yellow Box Store /. -->\n\n  <!-- Blue Box Products-->\n  <ion-card class=\"box box-blue\">\n    <ion-grid>\n      <ion-row>\n        <ion-col class=\"heading heading-white\" size=\"8\">Featured Products | Crockery</ion-col>\n        <ion-col><button>View All</button></ion-col>\n      </ion-row>\n    </ion-grid>\n    <ion-card>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n        <app-product-thumb></app-product-thumb>\n    </ion-card>\n  </ion-card>\n  <!-- ./ Blue Box Products /. -->\n\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/home/home.module.ts":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/home/home.page.ts");
/* harmony import */ var _commonviews_commonviews_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../commonviews/commonviews.module */ "./src/app/commonviews/commonviews.module.ts");








var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _commonviews_commonviews_module__WEBPACK_IMPORTED_MODULE_7__["CommonviewsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/home/home.page.scss":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n\nion-content ion-slides {\n  top: 0px;\n  z-index: 999;\n  margin: 0px 10px;\n  padding-bottom: 20px;\n  --bullet-background-active:#222e3d;\n  --bullet-background:#222e3d;\n}\n\nion-content ion-slides .swiper-pagination {\n  bottom: 0px !important;\n}\n\nion-content ion-slides .swiper-pagination .swiper-pagination-bullet {\n  opacity: 0.9;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvYXBwcy9yb3N0YWlsL3NyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURFRTtFQUNFLFFBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGtDQUFBO0VBQ0EsMkJBQUE7QUNDSjs7QURBSTtFQUNFLHNCQUFBO0FDRU47O0FERE07RUFDRSxZQUFBO0FDR1IiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndlbGNvbWUtY2FyZCBpbWcge1xuICBtYXgtaGVpZ2h0OiAzNXZoO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuaW9uLWNvbnRlbnR7XG4gIGlvbi1zbGlkZXN7XG4gICAgdG9wOiAwcHg7XG4gICAgei1pbmRleDogOTk5O1xuICAgIG1hcmdpbjogMHB4IDEwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gICAgLS1idWxsZXQtYmFja2dyb3VuZC1hY3RpdmU6IzIyMmUzZDtcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kOiMyMjJlM2Q7XG4gICAgLnN3aXBlci1wYWdpbmF0aW9ue1xuICAgICAgYm90dG9tOiAwcHggIWltcG9ydGFudDtcbiAgICAgIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXR7XG4gICAgICAgIG9wYWNpdHk6IDAuOTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn0iLCIud2VsY29tZS1jYXJkIGltZyB7XG4gIG1heC1oZWlnaHQ6IDM1dmg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbmlvbi1jb250ZW50IGlvbi1zbGlkZXMge1xuICB0b3A6IDBweDtcbiAgei1pbmRleDogOTk5O1xuICBtYXJnaW46IDBweCAxMHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbiAgLS1idWxsZXQtYmFja2dyb3VuZC1hY3RpdmU6IzIyMmUzZDtcbiAgLS1idWxsZXQtYmFja2dyb3VuZDojMjIyZTNkO1xufVxuaW9uLWNvbnRlbnQgaW9uLXNsaWRlcyAuc3dpcGVyLXBhZ2luYXRpb24ge1xuICBib3R0b206IDBweCAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgaW9uLXNsaWRlcyAuc3dpcGVyLXBhZ2luYXRpb24gLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldCB7XG4gIG9wYWNpdHk6IDAuOTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/home/home.page.ts":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomePage = /** @class */ (function () {
    function HomePage() {
    }
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=home-home-module-es5.js.map