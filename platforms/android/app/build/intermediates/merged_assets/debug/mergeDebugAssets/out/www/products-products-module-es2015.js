(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["products-products-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/products/product-detail/product-detail.component.html":
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/products/product-detail/product-detail.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-list Lines=\"none\" slot=\"start\">\n      <ion-item>\n        <h5><b>Men's Leather CASUAL SHOE RC2051 this is for test</b></h5>\n      </ion-item>\n    </ion-list>\n  </ion-toolbar>\n</ion-header>\n\n  <ion-content>\n    <!-- Banner -->\n    <ion-slides autoplay=\"5000\" loop=\"true\" speed=\"500\" pager=\"true\" class=\"home-slider-banner\">\n      <ion-slide>\n        <img src=\"https://mir-s3-cdn-cf.behance.net/project_modules/disp/806fe417784937.5676ca4a7b59a.jpg\"/>\n      </ion-slide>\n      <ion-slide>\n        <img src=\"https://mir-s3-cdn-cf.behance.net/project_modules/disp/9945f517784937.562c5a7b51769.jpg\"/>\n      </ion-slide>\n      <ion-slide>\n        <img src=\"https://mir-s3-cdn-cf.behance.net/project_modules/disp/fdaa0417784937.5676ca4a7c67e.jpg\"/>\n      </ion-slide>\n    </ion-slides>\n    <!-- ./ Banner /. -->\n    \n    <h4>Red Chief</h4>\n    <h5>Men's Leather CASUAL SHOE RC2051\n        <span class=\"off_float\">\n            <div class=\"main_off\">\n                <span class=\"off_container\">10% <br><span class=\"fs10\">OFF</span> </span>\n                <div id=\"burst-8\"></div>\n            </div>\n        </span>\n    </h5>\n    <ul class=\"dis-list\">\n        <li>\n          <span class=\"title\">name :</span>\n          <span class=\"value\">this is value</span>\n        </li>\n        <li>\n          <span class=\"title\">name :</span>\n          <span class=\"value\">this is value</span>\n        </li>\n        <li>\n          <span class=\"title\">name :</span>\n          <span class=\"value\">this is value</span>\n        </li>\n        <li>\n          <span class=\"title\">name :</span>\n          <span class=\"value\">this is value</span>\n        </li>\n        <li>\n          <span class=\"title\">name :</span>\n          <span class=\"value\">this is value</span>\n        </li>\n        <li>\n          <span class=\"title\">name :</span>\n          <span class=\"value\">this is value</span>\n        </li>\n        <li>\n          <span class=\"title\">name :</span>\n          <span class=\"value\">this is value</span>\n        </li>\n      </ul>\n    <ion-list Lines=\"none\">\n      <ion-item>\n        <ion-note slot=\"start\">\n          <ion-icon name=\"heart-empty\"></ion-icon> <span>Save</span> \n        </ion-note>\n        <ion-note slot=\"start\">\n          <ion-icon name=\"share\"></ion-icon> <span>Share</span> \n        </ion-note>\n        <ion-note slot=\"end\">\n        <span>MRP:</span>\n        <span class=\"price-old\">₹ 850</span>\n        <span class=\"price\">₹ 500</span>\n        </ion-note>\n      </ion-item>\n      <ion-item>\n        <ion-note slot=\"start\">\n          <span> BIGSAVE PRICE : ₹ 350 </span>\n        </ion-note>\n        <ion-note slot=\"end\">\n          <span class=\"text-success\"> In Stock </span>\n          </ion-note>\n      </ion-item>\n      <ion-item>\n        <ion-note slot=\"start\">\n          <span class=\"text-danger\">Returnable</span>\n        </ion-note>\n      </ion-item>\n      <ion-item class=\"counter\">\n        <ion-note class=\"input-val\" slot=\"start\">\n          <ion-input value=\"1\"></ion-input>\n        </ion-note>\n        <ion-note class=\"plus\" slot=\"start\">\n          <ion-icon name=\"add-circle-outline\"></ion-icon>\n        </ion-note>\n        <ion-note class=\"minus\" slot=\"start\">\n          <ion-icon name=\"remove-circle-outline\"></ion-icon>\n        </ion-note>\n      </ion-item>\n      <ion-item class=\"sizes\">\n        <ion-label>Size</ion-label>\n        <ion-select>\n          <ion-select-option value=\"\">Select Size</ion-select-option>\n          <ion-select-option value=\"dog\">Dog</ion-select-option>\n        </ion-select>\n      </ion-item>\n    </ion-list>\n    <ion-grid>\n      <ion-row>\n        <ion-col width-50><button class=\"button button-success\">Add To Cart</button></ion-col>\n        <ion-col width-50><button class=\"button button-info\">Go To Shop</button></ion-col>\n      </ion-row>\n    </ion-grid>\n  </ion-content>\n\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/products/product-list/product-list.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/products/product-list/product-list.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-list Lines=\"none\" slot=\"start\">\n      <ion-item>\n        <h5><b>Best Sellers</b></h5>\n      </ion-item>\n    </ion-list>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<!-- Yellow Box Products-->\n<ion-card class=\"box box-yellow\">\n  <ion-grid>\n    <ion-row>\n      <ion-col class=\"heading heading-white\" size=\"8\">Featured Products | Fashion</ion-col>\n      <ion-col><button>View All</button></ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-card>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n  </ion-card>\n</ion-card>\n<!-- ./ Yellow Box Products /. -->\n\n<!-- Blue Box Products-->\n<ion-card class=\"box box-blue\">\n  <ion-grid>\n    <ion-row>\n      <ion-col class=\"heading heading-white\" size=\"8\">Featured Products | Crockery</ion-col>\n      <ion-col><button>View All</button></ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-card>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n      <app-product-thumb></app-product-thumb>\n  </ion-card>\n</ion-card>\n<!-- ./ Blue Box Products /. -->\n</ion-content>"

/***/ }),

/***/ "./src/app/products/product-detail/product-detail.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/products/product-detail/product-detail.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-header h5 {\n  white-space: nowrap;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  width: 90%;\n  width: calc(100% - 112px);\n  font-size: 16px;\n  margin: -5px 23px 2px 0px;\n}\n\nion-content h4, ion-content h5 {\n  margin: 0px 10px !important;\n}\n\nion-content h5 {\n  border-bottom: 1px solid #bfbfbf;\n  padding-bottom: 5px;\n  font-weight: 600;\n  font-size: 16px;\n}\n\nion-content h5 .off_float {\n  float: right;\n  margin-right: 40px;\n}\n\nion-content h5 .off_float .main_off {\n  position: absolute;\n  z-index: 1;\n}\n\nion-content h5 .off_float .main_off .off_container {\n  position: absolute;\n  z-index: 999;\n  font-size: 12px;\n  top: 5px;\n  line-height: 11px;\n  left: 4px;\n  color: #fff;\n}\n\nion-content h5 .off_float .main_off #burst-8 {\n  background: #e40d35;\n  width: 30px;\n  height: 30px;\n  position: relative;\n  text-align: center;\n  -webkit-transform: rotate(20deg);\n          transform: rotate(20deg);\n}\n\nion-content h5 .off_float .main_off #burst-8:before {\n  content: \"\";\n  position: absolute;\n  top: 0;\n  left: 0;\n  height: 30px;\n  width: 30px;\n  background: #e40d35;\n  -webkit-transform: rotate(130deg);\n          transform: rotate(130deg);\n}\n\nh4 {\n  font-size: 14px;\n  font-weight: 100;\n  text-transform: uppercase;\n  font-style: italic;\n  margin-bottom: 5px !important;\n}\n\n.dis-list {\n  padding: 0;\n  margin: 10px 30px;\n}\n\n.dis-list li {\n  padding: 0;\n  line-height: 20px;\n  color: #666666;\n  font-size: 16px;\n}\n\n.dis-list li span {\n  display: inline-block;\n  font-size: 14px;\n}\n\n.dis-list li span.title {\n  font-weight: 500;\n  text-transform: uppercase;\n  margin-right: 10px;\n}\n\n.dis-list li span.value {\n  text-transform: capitalize;\n}\n\nion-slides {\n  top: 0px;\n  z-index: 999;\n  margin: 10px;\n  padding-bottom: 20px;\n  --bullet-background-active:#222e3d;\n  --bullet-background:#222e3d;\n}\n\nion-slides .swiper-pagination {\n  bottom: 0px !important;\n}\n\nion-slides .swiper-pagination .swiper-pagination-bullet {\n  opacity: 0.9;\n}\n\nion-list ion-note {\n  margin: 0px;\n  padding: 0px 5px;\n}\n\nion-list ion-note .text-success {\n  color: green;\n  font-weight: bold;\n}\n\nion-list ion-note .text-danger {\n  color: red;\n  font-weight: bold;\n}\n\nion-list .price {\n  font-size: 19px;\n  font-weight: bold;\n  text-align: right;\n  width: 50%;\n  margin-left: 5px;\n}\n\nion-list .price-old {\n  text-decoration: line-through;\n}\n\nion-list ion-icon {\n  margin-bottom: -4px;\n  margin-right: 3px;\n  font-size: 20px;\n  color: #5f5f5f;\n}\n\nion-list span {\n  font-size: 15px;\n  color: #5f5f5f;\n}\n\nion-list ion-input {\n  width: 35px;\n  height: 25px;\n  border: 1px solid #5f5f5f;\n  text-align: center;\n  border-radius: 5px;\n}\n\nion-list ion-item {\n  height: 30px;\n}\n\nion-list ion-item.counter {\n  width: calc(50% - 15px);\n  border: 1px solid #ddd;\n  height: 30px;\n  margin-left: 15px;\n  display: inline-block;\n}\n\nion-list ion-item.counter ion-note.plus,\nion-list ion-item.counter ion-note.minus {\n  margin-top: 3px;\n}\n\nion-list ion-item.counter ion-note {\n  padding: 0px;\n  width: -webkit-min-content;\n  width: -moz-min-content;\n  width: min-content;\n}\n\nion-list ion-item.counter ion-note ion-input {\n  padding-left: 12px !important;\n  border: none;\n  font-size: 14px;\n  font-weight: 600;\n}\n\nion-list ion-item.sizes {\n  width: calc(50% - 15px);\n  display: inline-block;\n  margin-right: 15px;\n  --min-height: 28px;\n  border: 1px solid #ddd;\n  border-left: none;\n  font-size: 14px;\n}\n\nion-list ion-item.sizes ion-label {\n  margin: 0px;\n}\n\nion-list ion-item.sizes ion-select {\n  padding: 0;\n}\n\nion-col .button {\n  width: 100%;\n  height: 30px;\n  color: white;\n  font-weight: bold;\n}\n\nion-col .button-success {\n  background: green;\n}\n\nion-col .button-info {\n  background: #0180fe;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvYXBwcy9yb3N0YWlsL3NyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC1kZXRhaWwvcHJvZHVjdC1kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3Byb2R1Y3RzL3Byb2R1Y3QtZGV0YWlsL3Byb2R1Y3QtZGV0YWlsLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNJO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0VBQ0EsVUFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0FDQVI7O0FES0E7RUFDSSwyQkFBQTtBQ0ZKOztBRElBO0VBQ0ksZ0NBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQ0ZKOztBREdJO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0FDRFI7O0FERVE7RUFDSSxrQkFBQTtFQUNBLFVBQUE7QUNBWjs7QURDWTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxRQUFBO0VBQ0EsaUJBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtBQ0NoQjs7QURDWTtFQUNJLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0NBQUE7VUFBQSx3QkFBQTtBQ0NoQjs7QURDWTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGlDQUFBO1VBQUEseUJBQUE7QUNDaEI7O0FES0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsNkJBQUE7QUNGSjs7QURJQTtFQUNJLFVBQUE7RUFDQSxpQkFBQTtBQ0RKOztBREVJO0VBQ0ksVUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7QUNBUjs7QURDUTtFQUNJLHFCQUFBO0VBQ0EsZUFBQTtBQ0NaOztBRENRO0VBQ0ksZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0FDQ1o7O0FEQ1E7RUFDSSwwQkFBQTtBQ0NaOztBREdBO0VBQ0ksUUFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQ0FBQTtFQUNBLDJCQUFBO0FDQUo7O0FEQ0k7RUFDRSxzQkFBQTtBQ0NOOztBREFNO0VBQ0UsWUFBQTtBQ0VSOztBREdJO0VBQ0ksV0FBQTtFQUNBLGdCQUFBO0FDQVI7O0FEQ1E7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7QUNDWjs7QURDUTtFQUNJLFVBQUE7RUFDQSxpQkFBQTtBQ0NaOztBREVJO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUNBUjs7QURFSTtFQUNJLDZCQUFBO0FDQVI7O0FERUk7RUFDSSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUNBUjs7QURFSTtFQUNJLGVBQUE7RUFDQSxjQUFBO0FDQVI7O0FERUk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQ0FSOztBREVJO0VBQ0ksWUFBQTtBQ0FSOztBREVJO0VBQ0ksdUJBQUE7RUFDQSxzQkFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0FDQVI7O0FEQ1E7O0VBRUksZUFBQTtBQ0NaOztBRENRO0VBQ0ksWUFBQTtFQUNBLDBCQUFBO0VBQUEsdUJBQUE7RUFBQSxrQkFBQTtBQ0NaOztBREFZO0VBQ0ksNkJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FDRWhCOztBREVJO0VBQ0ksdUJBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQ0FSOztBRENRO0VBQ0ksV0FBQTtBQ0NaOztBRENRO0VBQ0ksVUFBQTtBQ0NaOztBRElJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUNEUjs7QURHSTtFQUNJLGlCQUFBO0FDRFI7O0FER0k7RUFDSSxtQkFBQTtBQ0RSIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC1kZXRhaWwvcHJvZHVjdC1kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24taGVhZGVyIHtcbiAgICBoNSB7XG4gICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgICB3aWR0aDogOTAlO1xuICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gMTEycHgpO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIG1hcmdpbjogLTVweCAyM3B4IDJweCAwcHg7XG4gICAgfVxufVxuXG5pb24tY29udGVudCB7XG5oNCwgaDUge1xuICAgIG1hcmdpbjogMHB4IDEwcHggIWltcG9ydGFudDtcbn1cbmg1IHtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2JmYmZiZjtcbiAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIC5vZmZfZmxvYXQge1xuICAgICAgICBmbG9hdDogcmlnaHQ7XG4gICAgICAgIG1hcmdpbi1yaWdodDogNDBweDtcbiAgICAgICAgLm1haW5fb2ZmIHtcbiAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgIHotaW5kZXg6IDE7XG4gICAgICAgICAgICAub2ZmX2NvbnRhaW5lciB7XG4gICAgICAgICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICAgICAgICAgIHotaW5kZXg6IDk5OTtcbiAgICAgICAgICAgICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICAgICAgICAgICAgdG9wOiA1cHg7XG4gICAgICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDExcHg7XG4gICAgICAgICAgICAgICAgbGVmdDogNHB4O1xuICAgICAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgI2J1cnN0LTgge1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmQ6ICNlNDBkMzU7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMjBkZWcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgI2J1cnN0LTg6YmVmb3JlIHtcbiAgICAgICAgICAgICAgICBjb250ZW50OiBcIlwiO1xuICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgICAgICAgICB0b3A6IDA7XG4gICAgICAgICAgICAgICAgbGVmdDogMDtcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDMwcHg7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZDogI2U0MGQzNTtcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgxMzBkZWcpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxufVxufVxuaDR7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGZvbnQtd2VpZ2h0OiAxMDA7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBmb250LXN0eWxlOiBpdGFsaWM7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4ICFpbXBvcnRhbnQ7XG59XG4uZGlzLWxpc3R7XG4gICAgcGFkZGluZzowO1xuICAgIG1hcmdpbjogMTBweCAzMHB4O1xuICAgIGxpe1xuICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgICAgY29sb3I6ICM2NjY2NjY7ICAgIFxuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIHNwYW57XG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgICAgICBmb250LXNpemU6IDE0cHg7XG4gICAgICAgIH1cbiAgICAgICAgc3Bhbi50aXRsZXtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgICAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xuICAgICAgICB9XG4gICAgICAgIHNwYW4udmFsdWV7XG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICAgICAgfVxuICAgIH1cbn1cbmlvbi1zbGlkZXN7XG4gICAgdG9wOiAwcHg7XG4gICAgei1pbmRleDogOTk5O1xuICAgIG1hcmdpbjogMTBweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTojMjIyZTNkO1xuICAgIC0tYnVsbGV0LWJhY2tncm91bmQ6IzIyMmUzZDtcbiAgICAuc3dpcGVyLXBhZ2luYXRpb257XG4gICAgICBib3R0b206IDBweCAhaW1wb3J0YW50O1xuICAgICAgLnN3aXBlci1wYWdpbmF0aW9uLWJ1bGxldHtcbiAgICAgICAgb3BhY2l0eTogMC45O1xuICAgICAgfVxuICAgIH1cbn1cbmlvbi1saXN0IHtcbiAgICBpb24tbm90ZSB7XG4gICAgICAgIG1hcmdpbjogMHB4O1xuICAgICAgICBwYWRkaW5nOiAwcHggNXB4O1xuICAgICAgICAudGV4dC1zdWNjZXNzIHtcbiAgICAgICAgICAgIGNvbG9yOmdyZWVuO1xuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIH1cbiAgICAgICAgLnRleHQtZGFuZ2VyIHtcbiAgICAgICAgICAgIGNvbG9yOnJlZDtcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICB9XG4gICAgfVxuICAgIC5wcmljZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTlweDtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgICAgICB3aWR0aDogNTAlO1xuICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xuICAgIH1cbiAgICAucHJpY2Utb2xkIHtcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XG4gICAgfVxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogLTRweDtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAzcHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgY29sb3I6ICM1ZjVmNWY7XG4gICAgfVxuICAgIHNwYW4ge1xuICAgICAgICBmb250LXNpemU6IDE1cHg7XG4gICAgICAgIGNvbG9yOiAjNWY1ZjVmO1xuICAgIH1cbiAgICBpb24taW5wdXQge1xuICAgICAgICB3aWR0aDogMzVweDtcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjNWY1ZjVmO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICB9XG4gICAgaW9uLWl0ZW0ge1xuICAgICAgICBoZWlnaHQ6IDMwcHg7XG4gICAgfVxuICAgIGlvbi1pdGVtLmNvdW50ZXIge1xuICAgICAgICB3aWR0aDogY2FsYyg1MCUgLSAxNXB4KTtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2RkZDtcbiAgICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgICBtYXJnaW4tbGVmdDogMTVweDtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICBpb24tbm90ZS5wbHVzLFxuICAgICAgICBpb24tbm90ZS5taW51c3tcbiAgICAgICAgICAgIG1hcmdpbi10b3A6IDNweDtcbiAgICAgICAgfVxuICAgICAgICBpb24tbm90ZXtcbiAgICAgICAgICAgIHBhZGRpbmc6IDBweDtcbiAgICAgICAgICAgIHdpZHRoOiBtaW4tY29udGVudDtcbiAgICAgICAgICAgIGlvbi1pbnB1dHtcbiAgICAgICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDEycHggIWltcG9ydGFudDtcbiAgICAgICAgICAgICAgICBib3JkZXI6IG5vbmU7XG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiAxNHB4O1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgaW9uLWl0ZW0uc2l6ZXN7XG4gICAgICAgIHdpZHRoOiBjYWxjKDUwJSAtIDE1cHgpO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IFxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDE1cHg7XG4gICAgICAgIC0tbWluLWhlaWdodDogMjhweDtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2RkZDtcbiAgICAgICAgYm9yZGVyLWxlZnQ6IG5vbmU7XG4gICAgICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICAgICAgaW9uLWxhYmVse1xuICAgICAgICAgICAgbWFyZ2luOiAwcHg7XG4gICAgICAgIH1cbiAgICAgICAgaW9uLXNlbGVjdHtcbiAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgIH1cbiAgICB9XG59XG5pb24tY29sIHtcbiAgICAuYnV0dG9uIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGhlaWdodDogMzBweDtcbiAgICAgICAgY29sb3I6IHdoaXRlO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICB9XG4gICAgLmJ1dHRvbi1zdWNjZXNzIHtcbiAgICAgICAgYmFja2dyb3VuZDogZ3JlZW47XG4gICAgfVxuICAgIC5idXR0b24taW5mbyB7XG4gICAgICAgIGJhY2tncm91bmQ6ICMwMTgwZmU7XG4gICAgfVxufSIsImlvbi1oZWFkZXIgaDUge1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbiAgd2lkdGg6IDkwJTtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDExMnB4KTtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBtYXJnaW46IC01cHggMjNweCAycHggMHB4O1xufVxuXG5pb24tY29udGVudCBoNCwgaW9uLWNvbnRlbnQgaDUge1xuICBtYXJnaW46IDBweCAxMHB4ICFpbXBvcnRhbnQ7XG59XG5pb24tY29udGVudCBoNSB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjYmZiZmJmO1xuICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDE2cHg7XG59XG5pb24tY29udGVudCBoNSAub2ZmX2Zsb2F0IHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBtYXJnaW4tcmlnaHQ6IDQwcHg7XG59XG5pb24tY29udGVudCBoNSAub2ZmX2Zsb2F0IC5tYWluX29mZiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogMTtcbn1cbmlvbi1jb250ZW50IGg1IC5vZmZfZmxvYXQgLm1haW5fb2ZmIC5vZmZfY29udGFpbmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiA5OTk7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgdG9wOiA1cHg7XG4gIGxpbmUtaGVpZ2h0OiAxMXB4O1xuICBsZWZ0OiA0cHg7XG4gIGNvbG9yOiAjZmZmO1xufVxuaW9uLWNvbnRlbnQgaDUgLm9mZl9mbG9hdCAubWFpbl9vZmYgI2J1cnN0LTgge1xuICBiYWNrZ3JvdW5kOiAjZTQwZDM1O1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMjBkZWcpO1xufVxuaW9uLWNvbnRlbnQgaDUgLm9mZl9mbG9hdCAubWFpbl9vZmYgI2J1cnN0LTg6YmVmb3JlIHtcbiAgY29udGVudDogXCJcIjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGhlaWdodDogMzBweDtcbiAgd2lkdGg6IDMwcHg7XG4gIGJhY2tncm91bmQ6ICNlNDBkMzU7XG4gIHRyYW5zZm9ybTogcm90YXRlKDEzMGRlZyk7XG59XG5cbmg0IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogMTAwO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXN0eWxlOiBpdGFsaWM7XG4gIG1hcmdpbi1ib3R0b206IDVweCAhaW1wb3J0YW50O1xufVxuXG4uZGlzLWxpc3Qge1xuICBwYWRkaW5nOiAwO1xuICBtYXJnaW46IDEwcHggMzBweDtcbn1cbi5kaXMtbGlzdCBsaSB7XG4gIHBhZGRpbmc6IDA7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBjb2xvcjogIzY2NjY2NjtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuLmRpcy1saXN0IGxpIHNwYW4ge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5kaXMtbGlzdCBsaSBzcGFuLnRpdGxlIHtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuLmRpcy1saXN0IGxpIHNwYW4udmFsdWUge1xuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbn1cblxuaW9uLXNsaWRlcyB7XG4gIHRvcDogMHB4O1xuICB6LWluZGV4OiA5OTk7XG4gIG1hcmdpbjogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gIC0tYnVsbGV0LWJhY2tncm91bmQtYWN0aXZlOiMyMjJlM2Q7XG4gIC0tYnVsbGV0LWJhY2tncm91bmQ6IzIyMmUzZDtcbn1cbmlvbi1zbGlkZXMgLnN3aXBlci1wYWdpbmF0aW9uIHtcbiAgYm90dG9tOiAwcHggIWltcG9ydGFudDtcbn1cbmlvbi1zbGlkZXMgLnN3aXBlci1wYWdpbmF0aW9uIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXQge1xuICBvcGFjaXR5OiAwLjk7XG59XG5cbmlvbi1saXN0IGlvbi1ub3RlIHtcbiAgbWFyZ2luOiAwcHg7XG4gIHBhZGRpbmc6IDBweCA1cHg7XG59XG5pb24tbGlzdCBpb24tbm90ZSAudGV4dC1zdWNjZXNzIHtcbiAgY29sb3I6IGdyZWVuO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbmlvbi1saXN0IGlvbi1ub3RlIC50ZXh0LWRhbmdlciB7XG4gIGNvbG9yOiByZWQ7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuaW9uLWxpc3QgLnByaWNlIHtcbiAgZm9udC1zaXplOiAxOXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gIHdpZHRoOiA1MCU7XG4gIG1hcmdpbi1sZWZ0OiA1cHg7XG59XG5pb24tbGlzdCAucHJpY2Utb2xkIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XG59XG5pb24tbGlzdCBpb24taWNvbiB7XG4gIG1hcmdpbi1ib3R0b206IC00cHg7XG4gIG1hcmdpbi1yaWdodDogM3B4O1xuICBmb250LXNpemU6IDIwcHg7XG4gIGNvbG9yOiAjNWY1ZjVmO1xufVxuaW9uLWxpc3Qgc3BhbiB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgY29sb3I6ICM1ZjVmNWY7XG59XG5pb24tbGlzdCBpb24taW5wdXQge1xuICB3aWR0aDogMzVweDtcbiAgaGVpZ2h0OiAyNXB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjNWY1ZjVmO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cbmlvbi1saXN0IGlvbi1pdGVtIHtcbiAgaGVpZ2h0OiAzMHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0uY291bnRlciB7XG4gIHdpZHRoOiBjYWxjKDUwJSAtIDE1cHgpO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZGRkO1xuICBoZWlnaHQ6IDMwcHg7XG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5pb24tbGlzdCBpb24taXRlbS5jb3VudGVyIGlvbi1ub3RlLnBsdXMsXG5pb24tbGlzdCBpb24taXRlbS5jb3VudGVyIGlvbi1ub3RlLm1pbnVzIHtcbiAgbWFyZ2luLXRvcDogM3B4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0uY291bnRlciBpb24tbm90ZSB7XG4gIHBhZGRpbmc6IDBweDtcbiAgd2lkdGg6IG1pbi1jb250ZW50O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0uY291bnRlciBpb24tbm90ZSBpb24taW5wdXQge1xuICBwYWRkaW5nLWxlZnQ6IDEycHggIWltcG9ydGFudDtcbiAgYm9yZGVyOiBub25lO1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5pb24tbGlzdCBpb24taXRlbS5zaXplcyB7XG4gIHdpZHRoOiBjYWxjKDUwJSAtIDE1cHgpO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbi1yaWdodDogMTVweDtcbiAgLS1taW4taGVpZ2h0OiAyOHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZGRkO1xuICBib3JkZXItbGVmdDogbm9uZTtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuaW9uLWxpc3QgaW9uLWl0ZW0uc2l6ZXMgaW9uLWxhYmVsIHtcbiAgbWFyZ2luOiAwcHg7XG59XG5pb24tbGlzdCBpb24taXRlbS5zaXplcyBpb24tc2VsZWN0IHtcbiAgcGFkZGluZzogMDtcbn1cblxuaW9uLWNvbCAuYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMzBweDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbmlvbi1jb2wgLmJ1dHRvbi1zdWNjZXNzIHtcbiAgYmFja2dyb3VuZDogZ3JlZW47XG59XG5pb24tY29sIC5idXR0b24taW5mbyB7XG4gIGJhY2tncm91bmQ6ICMwMTgwZmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/products/product-detail/product-detail.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/products/product-detail/product-detail.component.ts ***!
  \*********************************************************************/
/*! exports provided: ProductDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductDetailComponent", function() { return ProductDetailComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ProductDetailComponent = class ProductDetailComponent {
    constructor() { }
    ngOnInit() { }
};
ProductDetailComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-detail',
        template: __webpack_require__(/*! raw-loader!./product-detail.component.html */ "./node_modules/raw-loader/index.js!./src/app/products/product-detail/product-detail.component.html"),
        styles: [__webpack_require__(/*! ./product-detail.component.scss */ "./src/app/products/product-detail/product-detail.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ProductDetailComponent);



/***/ }),

/***/ "./src/app/products/product-list/product-list.component.scss":
/*!*******************************************************************!*\
  !*** ./src/app/products/product-list/product-list.component.scss ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n\nion-content ion-slides {\n  top: 0px;\n  z-index: 999;\n  margin: 0px 10px;\n  padding-bottom: 20px;\n  --bullet-background-active:#222e3d;\n  --bullet-background:#222e3d;\n}\n\nion-content ion-slides .swiper-pagination {\n  bottom: 0px !important;\n}\n\nion-content ion-slides .swiper-pagination .swiper-pagination-bullet {\n  opacity: 0.9;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvYXBwcy9yb3N0YWlsL3NyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC1saXN0L3Byb2R1Y3QtbGlzdC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcHJvZHVjdHMvcHJvZHVjdC1saXN0L3Byb2R1Y3QtbGlzdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURFRTtFQUNFLFFBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGtDQUFBO0VBQ0EsMkJBQUE7QUNDSjs7QURBSTtFQUNFLHNCQUFBO0FDRU47O0FERE07RUFDRSxZQUFBO0FDR1IiLCJmaWxlIjoic3JjL2FwcC9wcm9kdWN0cy9wcm9kdWN0LWxpc3QvcHJvZHVjdC1saXN0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLndlbGNvbWUtY2FyZCBpbWcge1xuICBtYXgtaGVpZ2h0OiAzNXZoO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuaW9uLWNvbnRlbnR7XG4gIGlvbi1zbGlkZXN7XG4gICAgdG9wOiAwcHg7XG4gICAgei1pbmRleDogOTk5O1xuICAgIG1hcmdpbjogMHB4IDEwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XG4gICAgLS1idWxsZXQtYmFja2dyb3VuZC1hY3RpdmU6IzIyMmUzZDtcbiAgICAtLWJ1bGxldC1iYWNrZ3JvdW5kOiMyMjJlM2Q7XG4gICAgLnN3aXBlci1wYWdpbmF0aW9ue1xuICAgICAgYm90dG9tOiAwcHggIWltcG9ydGFudDtcbiAgICAgIC5zd2lwZXItcGFnaW5hdGlvbi1idWxsZXR7XG4gICAgICAgIG9wYWNpdHk6IDAuOTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiIsIi53ZWxjb21lLWNhcmQgaW1nIHtcbiAgbWF4LWhlaWdodDogMzV2aDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuaW9uLWNvbnRlbnQgaW9uLXNsaWRlcyB7XG4gIHRvcDogMHB4O1xuICB6LWluZGV4OiA5OTk7XG4gIG1hcmdpbjogMHB4IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuICAtLWJ1bGxldC1iYWNrZ3JvdW5kLWFjdGl2ZTojMjIyZTNkO1xuICAtLWJ1bGxldC1iYWNrZ3JvdW5kOiMyMjJlM2Q7XG59XG5pb24tY29udGVudCBpb24tc2xpZGVzIC5zd2lwZXItcGFnaW5hdGlvbiB7XG4gIGJvdHRvbTogMHB4ICFpbXBvcnRhbnQ7XG59XG5pb24tY29udGVudCBpb24tc2xpZGVzIC5zd2lwZXItcGFnaW5hdGlvbiAuc3dpcGVyLXBhZ2luYXRpb24tYnVsbGV0IHtcbiAgb3BhY2l0eTogMC45O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/products/product-list/product-list.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/products/product-list/product-list.component.ts ***!
  \*****************************************************************/
/*! exports provided: ProductListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductListComponent", function() { return ProductListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ProductListComponent = class ProductListComponent {
    constructor() { }
    ngOnInit() { }
};
ProductListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-product-list',
        template: __webpack_require__(/*! raw-loader!./product-list.component.html */ "./node_modules/raw-loader/index.js!./src/app/products/product-list/product-list.component.html"),
        styles: [__webpack_require__(/*! ./product-list.component.scss */ "./src/app/products/product-list/product-list.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ProductListComponent);



/***/ }),

/***/ "./src/app/products/products-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/products/products-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: ProductsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsRoutingModule", function() { return ProductsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _product_list_product_list_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./product-list/product-list.component */ "./src/app/products/product-list/product-list.component.ts");
/* harmony import */ var _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./product-detail/product-detail.component */ "./src/app/products/product-detail/product-detail.component.ts");





const routes = [
    { path: '', pathMatch: '', component: _product_list_product_list_component__WEBPACK_IMPORTED_MODULE_3__["ProductListComponent"] },
    { path: 'detail', pathMatch: '', component: _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_4__["ProductDetailComponent"] }
];
let ProductsRoutingModule = class ProductsRoutingModule {
};
ProductsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], ProductsRoutingModule);



/***/ }),

/***/ "./src/app/products/products.module.ts":
/*!*********************************************!*\
  !*** ./src/app/products/products.module.ts ***!
  \*********************************************/
/*! exports provided: ProductsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductsModule", function() { return ProductsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _products_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./products-routing.module */ "./src/app/products/products-routing.module.ts");
/* harmony import */ var _product_list_product_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./product-list/product-list.component */ "./src/app/products/product-list/product-list.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _commonviews_commonviews_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../commonviews/commonviews.module */ "./src/app/commonviews/commonviews.module.ts");
/* harmony import */ var _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./product-detail/product-detail.component */ "./src/app/products/product-detail/product-detail.component.ts");








let ProductsModule = class ProductsModule {
};
ProductsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_product_list_product_list_component__WEBPACK_IMPORTED_MODULE_4__["ProductListComponent"], _product_detail_product_detail_component__WEBPACK_IMPORTED_MODULE_7__["ProductDetailComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _products_routing_module__WEBPACK_IMPORTED_MODULE_3__["ProductsRoutingModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _commonviews_commonviews_module__WEBPACK_IMPORTED_MODULE_6__["CommonviewsModule"]
        ]
    })
], ProductsModule);



/***/ })

}]);
//# sourceMappingURL=products-products-module-es2015.js.map