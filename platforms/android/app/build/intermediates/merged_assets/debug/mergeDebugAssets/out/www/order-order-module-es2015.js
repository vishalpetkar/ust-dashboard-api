(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["order-order-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/order/cart/cart.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/order/cart/cart.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar>\n      <ion-buttons slot=\"start\">\n        <ion-menu-button></ion-menu-button>\n      </ion-buttons>\n      <ion-list Lines=\"none\" slot=\"start\">\n        <ion-item>\n          <h5><b>2 Items In Your Cart</b></h5>\n        </ion-item>\n      </ion-list>\n    </ion-toolbar>\n  </ion-header>\n  \n  <ion-content>\n    <ion-item Lines=\"none\">\n      <h4><b>Deliver to</b></h4>\n      <ion-button fill=\"outline\" slot=\"end\">Change</ion-button>\n    </ion-item>\n    <ion-item>\n      <h6>\n        VISHAL RAMESHWAR PETKAR <br>\n        260, Teka Naka, Nari Road, Sanyal Nagar, Nagpur, Maharashtra - 440026. <br>\n        Mobile : 09096221977\n      </h6>\n    </ion-item>\n\n    <ion-item style=\"--background: #e0ecfa;\">\n      <ion-avatar slot=\"start\">\n        <img src=\"https://www.rostail.com/assets/uploads/products/medium/vijay_book_depo.jpg\">\n      </ion-avatar>\n      <ion-label>\n        <h2 style=\"white-space: normal;\">Men's Leather CASUAL SHOE RC2051</h2>\n        <span>MRP:</span>\n        <span class=\"price-old\">₹ 850</span>\n        <span class=\"price\">₹ 500</span>\n        <div class=\"flex_row\">\n          <button class=\"button icon ion-minus-circled red\">\n            <ion-icon name=\"add\"></ion-icon>\n          </button>\n          <ion-input type=\"number\" value=\"333\"></ion-input>\n          <button class=\"button icon ion-plus-circled green\">\n            <ion-icon name=\"remove\"></ion-icon>\n          </button>\n        </div> \n      </ion-label>\n      \n    </ion-item>\n  </ion-content>\n  "

/***/ }),

/***/ "./src/app/order/cart/cart.component.scss":
/*!************************************************!*\
  !*** ./src/app/order/cart/cart.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-avatar {\n  border-radius: 0px;\n  margin: 0px;\n  height: 100px;\n  width: 150px;\n  margin-right: 10px;\n}\nion-avatar img {\n  border-radius: 0px;\n}\nh2, h3, p {\n  font-size: 14px;\n}\nh3 ion-icon {\n  font-size: 16px;\n  margin-bottom: -4px;\n}\nion-item {\n  margin: 10px;\n  margin-bottom: 6px;\n  margin-top: 0px;\n  border-radius: 5px;\n}\nh4, h6 {\n  margin: 0px 0px 10px 0px;\n  line-height: 22px;\n  color: #2d2d2d;\n}\n.price {\n  font-size: 19px;\n  font-weight: bold;\n  text-align: right;\n  width: 50%;\n  margin-left: 5px;\n}\n.price-old {\n  text-decoration: line-through;\n}\nion-item .qnt-input {\n  border: 1px solid #666;\n  margin-top: 10px;\n  border-radius: 5px;\n  width: 86%;\n}\nion-item .qnt-input button {\n  background: transparent;\n  color: black;\n  font-size: 20px;\n  padding: 0px 0px 0px 6px;\n  margin: 2px 0px 0px 1px;\n}\nion-item .qnt-input ion-input {\n  display: inline-block;\n  width: 50px;\n  height: 22px;\n  --padding-top: 2px;\n  --padding-bottom: 2px;\n  text-align: center;\n  font-weight: 500;\n}\nion-input {\n  text-align: center;\n}\n.button {\n  border-radius: 5px;\n  background: transparent;\n  font-size: 23px;\n  font-weight: bold;\n}\n.flex_row {\n  height: 35px;\n  width: 125px;\n  border: 2px solid #028600;\n  border-radius: 5px;\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n          flex-direction: row;\n  padding-top: 3px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvYXBwcy9yb3N0YWlsL3NyYy9hcHAvb3JkZXIvY2FydC9jYXJ0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9vcmRlci9jYXJ0L2NhcnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDQ0o7QURBSTtFQUNJLGtCQUFBO0FDRVI7QURFQTtFQUNJLGVBQUE7QUNDSjtBRENBO0VBQ0ksZUFBQTtFQUNBLG1CQUFBO0FDRUo7QURBQTtFQUVJLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQ0VKO0FEQUE7RUFDSSx3QkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQ0dKO0FEREE7RUFDSSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7RUFDQSxnQkFBQTtBQ0lKO0FERkE7RUFDSSw2QkFBQTtBQ0tKO0FERkk7RUFDSSxzQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0FDS1I7QURKUTtFQUNJLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSx3QkFBQTtFQUNBLHVCQUFBO0FDTVo7QURKWTtFQUNJLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFFQSxnQkFBQTtBQ0toQjtBREFBO0VBQ0ksa0JBQUE7QUNHSjtBREFBO0VBQ0ksa0JBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQ0dKO0FEREE7RUFDSSxZQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFFQSxvQkFBQTtFQUFBLGFBQUE7RUFFQSw4QkFBQTtFQUFBLDZCQUFBO1VBQUEsbUJBQUE7RUFDQSxnQkFBQTtBQ0lKIiwiZmlsZSI6InNyYy9hcHAvb3JkZXIvY2FydC9jYXJ0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWF2YXRhciB7XG4gICAgYm9yZGVyLXJhZGl1czogMHB4O1xuICAgIG1hcmdpbjogMHB4O1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgd2lkdGg6IDE1MHB4O1xuICAgIG1hcmdpbi1yaWdodDogMTBweDtcbiAgICBpbWcge1xuICAgICAgICBib3JkZXItcmFkaXVzOiAwcHg7XG4gICAgfVxufVxuXG5oMiwgaDMsIHAge1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbn1cbmgzIGlvbi1pY29uIHtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogLTRweDtcbn1cbmlvbi1pdGVtIHtcblxuICAgIG1hcmdpbjogMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiA2cHg7XG4gICAgbWFyZ2luLXRvcDogMHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbn1cbmg0LCBoNiB7XG4gICAgbWFyZ2luOiAwcHggMHB4IDEwcHggMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyMnB4O1xuICAgIGNvbG9yOiAjMmQyZDJkO1xufVxuLnByaWNlIHtcbiAgICBmb250LXNpemU6IDE5cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgd2lkdGg6IDUwJTtcbiAgICBtYXJnaW4tbGVmdDogNXB4O1xufVxuLnByaWNlLW9sZCB7XG4gICAgdGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XG59XG5pb24taXRlbSB7XG4gICAgLnFudC1pbnB1dHtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgIzY2NjtcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgICAgICB3aWR0aDogODYlO1xuICAgICAgICBidXR0b257XG4gICAgICAgICAgICBiYWNrZ3JvdW5kOnRyYW5zcGFyZW50O1xuICAgICAgICAgICAgY29sb3I6cmdiKDAsIDAsIDApO1xuICAgICAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgICAgICAgICAgcGFkZGluZzogMHB4IDBweCAwcHggNnB4O1xuICAgICAgICAgICAgbWFyZ2luOiAycHggMHB4IDBweCAxcHg7XG4gICAgICAgIH1cbiAgICAgICAgICAgIGlvbi1pbnB1dHtcbiAgICAgICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgICAgICAgICAgd2lkdGg6IDUwcHg7XG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAyMnB4O1xuICAgICAgICAgICAgICAgIC0tcGFkZGluZy10b3A6IDJweDtcbiAgICAgICAgICAgICAgICAtLXBhZGRpbmctYm90dG9tOiAycHg7XG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICAgICAgICAgIC8vIGZsb2F0OiBsZWZ0O1xuICAgICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgICAgICB9XG4gICAgfVxufVxuXG5pb24taW5wdXQge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmJ1dHRvbiB7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAgIGZvbnQtc2l6ZTogMjNweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5mbGV4X3JvdyB7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIHdpZHRoOiAxMjVweDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCAjMDI4NjAwO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICAtd2Via2l0LWZsZXgtZGlyZWN0aW9uOiByb3c7IFxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgcGFkZGluZy10b3A6IDNweDtcbn0iLCJpb24tYXZhdGFyIHtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICBtYXJnaW46IDBweDtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgd2lkdGg6IDE1MHB4O1xuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5pb24tYXZhdGFyIGltZyB7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbn1cblxuaDIsIGgzLCBwIHtcbiAgZm9udC1zaXplOiAxNHB4O1xufVxuXG5oMyBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgbWFyZ2luLWJvdHRvbTogLTRweDtcbn1cblxuaW9uLWl0ZW0ge1xuICBtYXJnaW46IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDZweDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbmg0LCBoNiB7XG4gIG1hcmdpbjogMHB4IDBweCAxMHB4IDBweDtcbiAgbGluZS1oZWlnaHQ6IDIycHg7XG4gIGNvbG9yOiAjMmQyZDJkO1xufVxuXG4ucHJpY2Uge1xuICBmb250LXNpemU6IDE5cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgd2lkdGg6IDUwJTtcbiAgbWFyZ2luLWxlZnQ6IDVweDtcbn1cblxuLnByaWNlLW9sZCB7XG4gIHRleHQtZGVjb3JhdGlvbjogbGluZS10aHJvdWdoO1xufVxuXG5pb24taXRlbSAucW50LWlucHV0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzY2NjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICB3aWR0aDogODYlO1xufVxuaW9uLWl0ZW0gLnFudC1pbnB1dCBidXR0b24ge1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgY29sb3I6IGJsYWNrO1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmc6IDBweCAwcHggMHB4IDZweDtcbiAgbWFyZ2luOiAycHggMHB4IDBweCAxcHg7XG59XG5pb24taXRlbSAucW50LWlucHV0IGlvbi1pbnB1dCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDUwcHg7XG4gIGhlaWdodDogMjJweDtcbiAgLS1wYWRkaW5nLXRvcDogMnB4O1xuICAtLXBhZGRpbmctYm90dG9tOiAycHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDUwMDtcbn1cblxuaW9uLWlucHV0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uYnV0dG9uIHtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgZm9udC1zaXplOiAyM3B4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmZsZXhfcm93IHtcbiAgaGVpZ2h0OiAzNXB4O1xuICB3aWR0aDogMTI1cHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMwMjg2MDA7XG4gIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xuICBkaXNwbGF5OiBmbGV4O1xuICAtd2Via2l0LWZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gIHBhZGRpbmctdG9wOiAzcHg7XG59Il19 */"

/***/ }),

/***/ "./src/app/order/cart/cart.component.ts":
/*!**********************************************!*\
  !*** ./src/app/order/cart/cart.component.ts ***!
  \**********************************************/
/*! exports provided: CartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartComponent", function() { return CartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let CartComponent = class CartComponent {
    constructor() { }
    ngOnInit() { }
};
CartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cart',
        template: __webpack_require__(/*! raw-loader!./cart.component.html */ "./node_modules/raw-loader/index.js!./src/app/order/cart/cart.component.html"),
        styles: [__webpack_require__(/*! ./cart.component.scss */ "./src/app/order/cart/cart.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], CartComponent);



/***/ }),

/***/ "./src/app/order/order-routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/order/order-routing.module.ts ***!
  \***********************************************/
/*! exports provided: OrderRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderRoutingModule", function() { return OrderRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _cart_cart_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cart/cart.component */ "./src/app/order/cart/cart.component.ts");




const routes = [
    { path: '', pathMatch: '', component: _cart_cart_component__WEBPACK_IMPORTED_MODULE_3__["CartComponent"] }
];
let OrderRoutingModule = class OrderRoutingModule {
};
OrderRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], OrderRoutingModule);



/***/ }),

/***/ "./src/app/order/order.module.ts":
/*!***************************************!*\
  !*** ./src/app/order/order.module.ts ***!
  \***************************************/
/*! exports provided: OrderModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderModule", function() { return OrderModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _order_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./order-routing.module */ "./src/app/order/order-routing.module.ts");
/* harmony import */ var _cart_cart_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./cart/cart.component */ "./src/app/order/cart/cart.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






let OrderModule = class OrderModule {
};
OrderModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_cart_cart_component__WEBPACK_IMPORTED_MODULE_4__["CartComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _order_routing_module__WEBPACK_IMPORTED_MODULE_3__["OrderRoutingModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"]
        ]
    })
], OrderModule);



/***/ })

}]);
//# sourceMappingURL=order-order-module-es2015.js.map